"""
Miscellaneous functions for neural networks

Original Code Source: 
    [1] http://deeplearning.net/tutorial/code/utils.py
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3
"""

#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import os

#%%
def merge_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.'''
    z = x.copy()
    z.update(y)
    return z

#%%
def save_dict(filename,dd):
    """ Given dictionary, saves file with keys and values """
    with open(filename, 'w') as f:
        for key in dd.keys():
            f.write(unicode(key) + u' : ' + unicode(dd[key])+'\n')

#%% 
def find_display_dim(dim):
    """ Finds dimensions for tiling images """
    x = np.floor(np.sqrt(dim))
    IS_bad = True
    while IS_bad:
        assert x>0
        y = dim/x
        if np.remainder(dim,x)==0:
            IS_bad=False
        else:
            x = x-1
            
    return (int(x),int(y))


#%% 
def safe_make_folders(save_folder):
    """ Safely creates folder without overwriting data """

    # If folder doesn't exist, make it
    if not os.path.isdir(save_folder):
        os.makedirs(save_folder)
    else:
        # If folder does exist, check if has other folders
        # other files are okay, for example > out.txt
        dir_list = [ dd for dd in os.listdir(save_folder) 
                        if os.path.isdir(os.path.join(save_folder, dd))]
        
        if ( len(dir_list)==1) and (dir_list[0]=='pretrain_dbn'):
            # This is an acceptable folder
            pass
        elif len(dir_list)>0:
            # Would rather have program terminate than overwrite data
            os.makedirs(save_folder)

#%%
def scale_to_unit_interval(ndar, eps=1e-8):
    """ 
    Scales all values in the ndarray ndar to be between 0 and 1 
    Copied from http://deeplearning.net/tutorial/code/utils.py     
    """
    ndar = ndar.copy()
    ndar -= ndar.min()
    ndar *= 1.0 / (ndar.max() + eps)
    return ndar
               
#%%
def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                       scale_rows_to_unit_interval=True,
                       output_pixel_vals=True):
    """
    Copied from http://deeplearning.net/tutorial/code/utils.py    
    
    Transform an array with one flattened image per row, into an array in
    which images are reshaped and layed out like tiles on a floor.

    This function is useful for visualizing datasets whose rows are images,
    and also columns of matrices for transforming those rows
    (such as the first layer of a neural net).

    :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
    be 2-D ndarrays or None;
    :param X: a 2-D array in which every row is a flattened image.

    :type img_shape: tuple; (height, width)
    :param img_shape: the original shape of each image

    :type tile_shape: tuple; (rows, cols)
    :param tile_shape: the number of images to tile (rows, cols)

    :param output_pixel_vals: if output should be pixel values (i.e. int8
    values) or floats

    :param scale_rows_to_unit_interval: if the values need to be scaled before
    being plotted to [0,1] or not


    :returns: array suitable for viewing as an image.
    (See:`Image.fromarray`.)
    :rtype: a 2-d array with same dtype as X.

    """

    assert len(img_shape) == 2
    assert len(tile_shape) == 2
    assert len(tile_spacing) == 2

    out_shape = [(ishp + tsp) * tshp - tsp
        for ishp, tshp, tsp in zip(img_shape, tile_shape, tile_spacing)]

    if isinstance(X, tuple):
        assert len(X) == 4
        # Create an output np ndarray to store the image
        if output_pixel_vals:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype='uint8')
        else:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype=X.dtype)

        #colors default to 0, alpha defaults to 1 (opaque)
        if output_pixel_vals:
            channel_defaults = [0, 0, 0, 255]
        else:
            channel_defaults = [0., 0., 0., 1.]

        for i in range(4):
            if X[i] is None:
                # if channel is None, fill it with zeros of the correct
                # dtype
                dt = out_array.dtype
                if output_pixel_vals:
                    dt = 'uint8'
                out_array[:, :, i] = np.zeros(
                    out_shape,
                    dtype=dt
                ) + channel_defaults[i]
            else:
                # use a recurrent call to compute the channel and store it
                # in the output
                out_array[:, :, i] = tile_raster_images(
                    X[i], img_shape, tile_shape, tile_spacing,
                    scale_rows_to_unit_interval, output_pixel_vals)
        return out_array

    else:
        # if we are dealing with only one channel
        H, W = img_shape
        Hs, Ws = tile_spacing

        # generate a matrix to store the output
        dt = X.dtype
        if output_pixel_vals:
            dt = 'uint8'
        out_array = np.zeros(out_shape, dtype=dt)

        for tile_row in range(tile_shape[0]):
            for tile_col in range(tile_shape[1]):
                if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                    this_x = X[tile_row * tile_shape[1] + tile_col]
                    if scale_rows_to_unit_interval:
                        # if we should scale values to be between 0 and 1
                        # do this by calling the `scale_to_unit_interval`
                        # function
                        this_img = scale_to_unit_interval(
                                        this_x.reshape(img_shape))
                    else:
                        this_img = this_x.reshape(img_shape)
                    # add the slice to the corresponding position in the
                    # output array
                    c = 1
                    if output_pixel_vals:
                        c = 255
                    out_array[tile_row * (H + Hs): tile_row * (H + Hs) + H,
                              tile_col * (W + Ws): tile_col * (W + Ws) + W
                              ] = this_img * c
        return out_array