"""
The paparazzi take lots of snapshots during learning

Two sets of snapshots: per epoch and all epochs

Written by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

Useful references
[1] http://deeplearning.net/tutorial/rbm.html
[2] Hinton Guide to RBM - https://www.cs.toronto.edu/~hinton/absps/guideTR.pdf

Most of measures are from [2], pseudolikelihood is from [1]. Many measures
are my own personal preference.
"""

#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
# Force matplotlib to not use any Xwindows backend.
# Otherwise will get error on linux servers
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import theano
import theano.tensor as TT
import os
try:
    import PIL.Image as Image
except ImportError:
    import Image
from sklearn.metrics import confusion_matrix, classification_report
from scipy.linalg import svd
import my_data
import logistic_sgd
import utils
import ais
import sample_nnet

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

#%% 
class Paparazzi(object):
    """ Object to keep track of nnet stats during training """
    def __init__(self, nnet_obj, data, mom_mb, lr_mb, 
                     n_epochs_early, save_folder, keep_snapshots = True):          
        """
        If keep_snapshots = True, records details per epoch, epoch text report,
            and extra info at special epochs
        If keep_snapshots = False, only issues epoch text report
        """
        assert type(data) is my_data.Data
                
        self.train_class = nnet_obj.train_class                

        ####### Implicit Parameters         
        # hidden act, all examples led to uniterpretable image
        self.n_ex_hid_act = 50              
                
        # Logreg
        # these parameters are minimially optimized on MNIST
        # I just want a rough estimate of accuracy
        self.logreg_mbs = 1000
        self.logreg_lr = 0.3
        self.logreg_max_epochs = 250                  
        #######

        self.n_h_layers = nnet_obj.n_layers-1 
        self.nnet_obj = nnet_obj
        self.data = data        
        self.epoch_mom = mom_mb[0::data.train.n_batches]
        self.epoch_lr = lr_mb[0::data.train.n_batches]
        self.n_epochs_early = n_epochs_early
        self.save_folder = save_folder
        self.keep_snapshots = keep_snapshots
        self.n_samples = self.data.train.n_samples
     
        # Makes all the folders to save results
        utils.safe_make_folders(save_folder)
    
        # Keep track of best statistics
        self.best_cost = float('inf')       
        
        # Record things needed in text_epoch
        self.cost = 0.0

        
        if self.train_class == 'unsuper':
            ####### Implicit Parameters         
        
            # Sampling parameters
            self.n_each = 2 # num ex for sampling
            self.n_samples = 10 # number of samples
            self.plot_every = 2000 # number of gibbs steps to skip            
        
            # Ais
            self.n_runs_detailed = np.int32(1e3)
            self.n_betas_detailed = np.int32(1.5e4) # seems like enough beta for unbiased sampling
            #######            
            
            
            self.best_fe_diff = float('inf')
            self.best_recon = float('inf')
            self.best_pslike = -float('inf')
            self.fe_diff = 0.0
            self.pslike = 0.0
            self.recon = 0.0

            # function to calculate pslike
            up_to = TT.iscalar()
            seq = TT.arange(up_to)
            outputs_info = TT.as_tensor_variable(np.asarray(0.0))
            scan_result, scan_updates = theano.scan(
                                            nnet_obj.pseudo_likelihood_cost_examples,
                                                outputs_info=outputs_info,
                                                sequences=seq)
            self.pslike_fxn = theano.function(inputs=[up_to], 
                                              updates = scan_updates,
                                              outputs=scan_result[-1])
        elif self.train_class == 'super':
            self.best_errors = float('inf') 
            self.errors = 0.0


        if keep_snapshots:       
            # Make folders for each type of plot
            os.makedirs(self.save_folder + '/mb_details')
            os.makedirs(self.save_folder + '/costs')

            # Records stats over all epochs
            self.epoch_cost = []
            self.epoch_norm_cost = []
            self.epoch_errors = []
            
            # Only at detailed points
            self.detailed_epoch = []

            for i in range(self.n_h_layers):
                os.makedirs(self.save_folder + '/accuracy_h'+str(i+1))
                os.makedirs(self.save_folder + '/active_field_h'+str(i+1))
                os.makedirs(self.save_folder + '/hidden_activation_h'+str(i+1))
                os.makedirs(self.save_folder + '/receptive_field_h'+str(i+1))
                os.makedirs(self.save_folder + '/svd_h'+str(i+1))
                
                filename = self.save_folder + '/accuracy_h'+str(i+1)+'/parameters.txt'
                param_dict = {'logreg_mbs' : self.logreg_mbs, 
                              'logreg_lr' : self.logreg_lr,
                              'logreg_max_epochs' : self.logreg_max_epochs}
                utils.save_dict(filename,param_dict) 


            if self.train_class == 'unsuper':
                os.makedirs(self.save_folder + '/samples')
                os.makedirs(self.save_folder + '/ais')
                
                filename = self.save_folder + '/samples/parameters.txt'
                param_dict = {'n_each' : self.n_each, 'n_samples' : self.n_samples,
                      'plot_every' : self.plot_every}
                utils.save_dict(filename,param_dict)
                
                filename = self.save_folder + '/ais/parameters.txt'
                param_dict = {'n_runs_detailed' : self.n_runs_detailed,
                              'n_betas_detailed' : self.n_betas_detailed}
                utils.save_dict(filename,param_dict)

                # Make AIS objects
                # Needed to make these objects once otherwise pay too high of
                # timing cost to compile the necessary theano fxns
                self.ais_detailed = ais.AIS(self.nnet_obj, self.data, 
                                            n_runs = self.n_runs_detailed, 
                                            n_betas = self.n_betas_detailed)

                # Records stats over all epochs
                self.epoch_pslike = []
                self.epoch_recon = []
                self.epoch_fe_diff  = []
                
                # Records detailed ais stats
                self.detailed_logz = []
                self.detailed_logz_10 = []
                self.detailed_logz_90 = []
                self.detailed_logpvalid = []
                self.detailed_logpvalid_low = []
                self.detailed_logpvalid_high = []

            elif self.train_class == 'super':
                self.detailed_valid_cost = []
                self.detailed_valid_errors = []
           

    #%%
    def stats_update(self,all_cost,all_cost2,epoch):
        """ Each epoch updates need stats"""

        self.all_cost = all_cost  
        self.cost = np.mean(all_cost)       

        if self.train_class == 'unsuper':
            self.all_recon = all_cost2 
            self.recon =  np.mean(all_cost2)        
            # Calc pslike
            self.pslike = self.pslike_fxn(self.data.n_input).item(0)                                       
                                  
            # free energy of validation vs train
            valid_fe = self.nnet_obj.free_energy(self.data.valid.data)
            avg_valid_fe =  np.mean(valid_fe.eval())
            train_fe = self.nnet_obj.free_energy(self.data.train.data)
            avg_train_fe =  np.mean(train_fe.eval())
            self.fe_diff = avg_valid_fe - avg_train_fe   
            
        elif self.train_class == 'super':
            self.all_errors = all_cost2
            self.errors = np.mean(all_cost2)
            
        if self.keep_snapshots:
            
            # calculate norm cost
            self.epoch_norm_cost += [self.nnet_obj.get_norm_cost(want_np=True)]        

            # Record stuff for plotting
            self.epoch_cost += [self.cost]
            if self.train_class == 'unsuper':
                self.epoch_pslike += [self.pslike]
                self.epoch_recon += [self.recon]
                self.epoch_fe_diff += [self.fe_diff]
            elif self.train_class == 'super':
                self.epoch_errors += [self.errors]
            
        
        # Check if there is a new best
        # Early epochs with changing momentum are not representative
        if epoch>self.n_epochs_early:        
            if self.cost < self.best_cost:
                self.best_cost = self.cost
                if self.train_class == 'super':
                    self.nnet_obj.update_best(epoch)

            if self.train_class == 'unsuper':
                if self.fe_diff < self.best_fe_diff:        
                    self.best_fe_diff = self.fe_diff
                if self.recon < self.best_recon:
                    self.best_recon = self.recon
                if self.pslike > self.best_pslike:
                    self.best_pslike = self.pslike
                    self.nnet_obj.update_best(epoch)
            elif self.train_class == 'super':
                if self.errors < self.best_errors:
                    self.best_errors = self.errors

    #%%
    def text_epoch(self,epoch):
        """ 
        Print out training details to the screen.
        Always do this, even if keep_snapshots=False                
        """
     
        print('Training epoch {} details'.format(epoch))
        if self.train_class == 'unsuper':
            text = 'Avg free energy valid minus train {:0.3f}, best is {:0.3f}'
            print(text.format(self.fe_diff,self.best_fe_diff))
            text = 'Total pseudo cost on training is {:0.3f}, best is {:0.3f}' 
            print(text.format(self.cost,self.best_cost))
            text = 'Pseuolikelihood cost on training is {:0.3f}, best is {:0.3f}' 
            print(text.format(self.pslike,self.best_pslike))
            text = 'Reconstruction cost on training is {:0.3f}, best is {:0.3f}'        
            print(text.format(self.recon,self.best_recon)) 
        elif self.train_class == 'super':
            text = 'Total cost on training is {:0.3f}, best is {:0.3f}' 
            print(text.format(self.cost,self.best_cost))
            text = 'Percent training error is {:0.3f}, best is {:0.3f}'
            print(text.format(self.errors*100,self.best_errors*100))
              
            
        for i in range(self.n_h_layers):
            # Records relative weight changes
            value_vel_W = np.abs(self.nnet_obj.vel_ls[i].get_value())
            value_W = np.abs(self.nnet_obj.weight_ls[i].get_value())
            rel_ch = (value_vel_W/value_W).flatten()
            text1 = 'Percent of rel W'+str(i)+str(i+1)+' change less than:'
            text =' 1e-4 = {:2.1f}%, 1e-3 = {:2.1f}%, 1e-2 = {:2.1f}%'
            norm = 100.0/rel_ch.size
            print(text1+text.format(np.sum(rel_ch<1e-4)*norm,
                              np.sum(rel_ch<1e-3)*norm,
                              np.sum(rel_ch<1e-2)*norm))
    
        # Preps for activations of each layer
        index = self.data.train.get_index_examples(self.n_ex_hid_act)
        self.act_out = self.nnet_obj.output_act(self.data.train.data[index])
        for i in range(self.n_h_layers):
            # Records sizes of activations at each layer
            act = self.act_out[i]
            print('Percent of hidden layer '+str(i+1)+' activation less than: ')
            text ='    0.01 = {:2.1f}%, 0.1 = {:2.1f}%, 0.5 = {:2.1f}%, 0.9 = {:2.1f}%, 0.99 = {:2.1f}%'
            norm = 100.0/act.size
            print(text.format(np.sum(act<0.01)*norm,
                              np.sum(act<0.1)*norm,
                              np.sum(act<0.5)*norm,
                              np.sum(act<0.9)*norm,
                              np.sum(act<0.99)*norm))
        
        # Space                      
        print('')

    #%%
    def snapshots_epoch(self, epoch, valid_cost=None, valid_errors=None):
        """ Wrapper for snapshots at detailed epoch points """
        
        if self.train_class == 'unsuper':
            assert valid_cost is None
            assert valid_errors is None
            self.snapshots_epoch_nnet_unsuper(epoch)
        elif self.train_class == 'super':
            assert valid_cost is not None
            assert valid_errors is not None
            self.snapshots_epoch_nnet_super(epoch, valid_cost, valid_errors)
        
        for i in range(self.n_h_layers):
            self.snapshots_hidden_layer(epoch, i+1)
       
    #%%
    def snapshots_epoch_nnet_super(self,epoch, valid_cost, valid_errors):
        """ 
        These are snapshots for the whole neural network and not layer dependent
        """        
        self.detailed_epoch += [epoch]
        self.detailed_valid_cost += [valid_cost]
        self.detailed_valid_errors += [valid_errors] 

    #%%
    def snapshots_epoch_nnet_unsuper(self,epoch):
        """ 
        These are snapshots for the whole neural network and not layer dependent
        """        
        
        # Detailed AIS run
        logz, logz_90, logz_10 = self.ais_detailed.run()
        valid_fe = self.nnet_obj.free_energy(self.data.valid.data)
        avg_valid_fe =  np.mean(valid_fe.eval())
        logpvalid = avg_valid_fe - logz
        logpvalid_high = avg_valid_fe - logz_10
        logpvalid_low = avg_valid_fe - logz_90
        
        # Records detailed ais stats
        self.detailed_epoch += [epoch]
        self.detailed_logz += [logz]
        self.detailed_logz_10 += [logz_10]
        self.detailed_logz_90 += [logz_90]
        self.detailed_logpvalid += [logpvalid]
        self.detailed_logpvalid_low += [logpvalid_low]
        self.detailed_logpvalid_high += [logpvalid_high]
        
        # save ais data
        filename = self.save_folder + '/ais/log_z_detailed.txt'  
        out_ls = [logz, logz_90, logz_10]      
        self.write_ais_table(filename,epoch,out_ls)
        
        filename = self.save_folder + '/ais/log_p_valid_detailed.txt'  
        out_ls = [logpvalid, logpvalid_high, logpvalid_low]      
        self.write_ais_table(filename,epoch,out_ls)
        
        # samples from model
        samples = sample_nnet.sample_nnet(self.nnet_obj, self.data, 
                            self.n_each, self.n_samples, self.plot_every)
        self.plot_samples(samples,epoch)
    
    #%%
    @staticmethod
    def write_ais_table(filename,epoch,out_ls):
        """ Saves AIS output to txt """
        if not os.path.exists(filename):
            with open(filename, 'w'):
                pass          
        with open(filename, 'a') as f:
            out_matrix = np.array([epoch]+out_ls)
            ss = out_matrix.size*'{}\t'+'\n'            
            f.write(unicode(ss.format(*out_matrix)))    
    

    #%%
    def snapshots_hidden_layer(self,epoch,layer):
        """ 
        These are snapshots of a given hidden layer
        """   

        # Prob of hidden act on ordered input
        act = self.act_out[layer-1]    
    
        # get predictions
        load_network_dict = {'nnet':self.nnet_obj,'data':self.data}
        output_data = my_data.Data('neural_network', mbs = self.logreg_mbs,
                                   load_network_dict = load_network_dict,
                                   hid_layer = layer)
        (train_predict, valid_predict, test_predict
        )  = logistic_sgd.sgd_optimization(
                output_data, lr = self.logreg_lr, 
                max_epochs = self.logreg_max_epochs, mbs = self.logreg_mbs)

        if layer==1:
            avg_data = self.data.train.avg_data
        else:
            act_avg = self.nnet_obj.output_act(self.data.train.avg_data) 
            avg_data = act_avg[layer-2]  
                
        self.accuracy_tables(train_predict, valid_predict, epoch, layer)

        if self.train_class == 'unsuper':
            W = self.nnet_obj.synapses_ls[layer-1].output_W()
        elif self.train_class == 'super':
            W = self.nnet_obj.layers_ls[layer].output_W()
            
        x, y = utils.find_display_dim(W.shape[0])
        
        self.plot_hid_act(act,epoch,layer)  
        self.plot_svd(W,epoch,layer)
        self.plot_receptive_fields(x,y,W,epoch,layer)            
        self.plot_active_fields(x,y,W,avg_data,epoch,layer)
  
       
    #%%
    def accuracy_tables(self,train_predict,valid_predict,epoch,layer):
        """ 
        This calculates accuracy of logistic regression of top layer outputs.
        """       
        train_lbl = self.data.train.lbl.eval()
        valid_lbl = self.data.valid.lbl.eval()
    
        save_dir = self.save_folder + '/accuracy_h' + str(layer)
        
        def write_class_report(filepath,epoch,lbl,predict):    
            # Save precision and recall
            # either create or open up a file for storing accuracy
            if not os.path.exists(filepath):
                with open(filepath, 'w'):
                    pass          
            with open(filepath, 'a') as f:
                f.write(u'Epoch {}\n'.format(epoch))
                ss = classification_report(lbl,predict,digits=4)
                f.write(unicode(ss))
                f.write(u'\n----------------------------------------------------\n')        
            
            
        filepath = save_dir + '/train_report.txt'    
        write_class_report(filepath,epoch,train_lbl,train_predict)   
        filepath = save_dir + '/valid_report.txt'    
        write_class_report(filepath,epoch,valid_lbl,valid_predict)          
        
        # Look into mistakes
        train_confuse = confusion_matrix(train_lbl,train_predict)
        valid_confuse = confusion_matrix(valid_lbl,valid_predict)
        # These are now just the errors    
        np.fill_diagonal(train_confuse,0)
        np.fill_diagonal(valid_confuse,0)
        cat = np.unique(train_lbl).astype('int')
        
        def write_confuse_matrix(filename,epoch,cat,confuse):        
            # Save error matrix of training  
            # either create or open up a file for storing accuracy
            if not os.path.exists(filename):
                with open(filename, 'w'):
                    pass          
            with open(filename, 'a') as f:
                f.write(u'Epoch {}. Row is truth and col is guess.\n\n'.format(epoch))        
                ss = '\t'+cat.size*'{}\t'+'\n'
                f.write(unicode(ss.format(*cat)))
                
                out_matrix = np.hstack((cat.reshape((cat.size,1)),confuse))
                ss = out_matrix.shape[1]*'{}\t'+'\n'            
                for i in range(cat.shape[0]):
                    f.write(unicode(ss.format(*out_matrix[i,:])))
                f.write(u'\n----------------------------------------------------\n\n')
    
        filename = save_dir + '/train_errors.txt'  
        write_confuse_matrix(filename,epoch,cat,train_confuse)
        filename = save_dir + '/valid_errors.txt'  
        write_confuse_matrix(filename,epoch,cat,valid_confuse)
  
    #%%
    def plot_samples(self,samples,epoch):
        """ Plots gibbs samples from neural network """    
         
        image = Image.fromarray(samples)
        save_dir = self.save_folder + '/samples'
        image.save(save_dir + '/epoch{}_samples.pdf'.format(epoch))        
    
    #%%
    def plot_hid_act(self,act,epoch,layer):
        """ Plots hidden activation. Do not want worthless units that are
        always on or always off. These are vertical stripes
        """
        save_dir = self.save_folder + '/hidden_activation_h' +str(layer)
        fig = plt.figure()
        plt.imshow(act,cmap='gray',interpolation='none')
        plt.tick_params(axis='both', which='both', bottom='off', top='off', 
                        labelbottom='off', right='off', 
                        left='off', labelleft='off')
        plt.title('Act hidden units. {} ex/category.'.format(self.n_ex_hid_act))
        plt.xlabel('Each column is a hidden unit')
        plt.ylabel('Each row is input data, sorted by category')
        plt.colorbar()
        savename = save_dir + '/epoch{}_hidden_act.pdf'.format(epoch)                
        fig.savefig(savename, bbox_inches='tight')
        plt.close()

    #%%
    def plot_svd(self,W,epoch,layer):
        """ 
        Plots SVD details of weight matrix. I find this informative 
        as to the types of interactions that are learned.
        Participation ratio measures the spread of the basis vectors.    
        """
        
        save_dir = self.save_folder + '/svd_h'+str(layer) 
        
        n_v, n_h = W.shape  
        u,s,vh = svd(W)
        v = vh.T    
    
        # plot u 
        fig = plt.figure()
        plt.imshow(u,cmap='gray',interpolation='none')
        plt.axis('off')
        plt.title('SVD input basis')
        plt.colorbar()
        savename = save_dir + '/epoch{}_u.pdf'.format(epoch)                
        fig.savefig(savename, bbox_inches='tight')
        plt.close()
    
        # Plot v
        fig = plt.figure()
        plt.imshow(v,cmap='gray',interpolation='none')
        plt.axis('off')
        plt.title('SVD output basis')
        plt.colorbar()
        savename = save_dir + '/epoch{}_v.pdf'.format(epoch)                
        fig.savefig(savename, bbox_inches='tight')
        plt.close()
        
        x = range(s.size)
    
        # Plot singular values
        fig = plt.figure()
        plt.scatter(x,s)
        plt.title('SVD S values')
        savename = save_dir + '/epoch{}_s.pdf'.format(epoch)                
        fig.savefig(savename, bbox_inches='tight')
        plt.close()
        
        # Plot singular vlaues on log scale
        fig = plt.figure()
        plt.scatter(x,np.log10(s))
        plt.title('SVD log10(S) values')
        savename = save_dir + '/epoch{}_log_s.pdf'.format(epoch)
        fig.savefig(savename, bbox_inches='tight') 
        plt.close()    
        
        # Calculate participation ratio of U
        p_u_norm = np.repeat(np.sum(np.abs(u),axis=0).reshape((1,n_v)),n_v,axis=0)
        p_u = np.abs(u)/p_u_norm   
        pr_u = np.sum(p_u**2,axis=1)  
        x = range(n_v)
        
        # inverse particpation percent, max is 1, min is 1/N
        fig = plt.figure()
        plt.scatter(x,1/pr_u/n_v) 
        plt.ylim((0,1))
        plt.title('Inv. part. per. of input. 1 = all, min is 1/N.')
        savename = save_dir + '/epoch{}_ipr_u.pdf'.format(epoch)
        fig.savefig(savename, bbox_inches='tight')
        plt.close()
        
        # Calculate participation ratio of U    
        p_v_norm = np.repeat(np.sum(np.abs(v),axis=0).reshape((1,n_h)),n_h,axis=0)
        p_v = np.abs(v)/p_v_norm   
        pr_v = np.sum(p_v**2,axis=1)  
        x = range(n_h)
        
        # inverse particpation percent, max is 1, min is 1/N
        fig = plt.figure()
        plt.scatter(x,1/pr_v/n_h) 
        plt.ylim((0,1))        
        plt.title('Inv. part. per. of output. 1 = all, min is 1/N.')
        savename = save_dir + '/epoch{}_ipr_v.pdf'.format(epoch)
        fig.savefig(savename, bbox_inches='tight')
        plt.close()


    #%%
    def plot_receptive_fields(self,x,y,W,epoch,layer):
        """ Plots receptive fields """
    
        tile_shape = utils.find_display_dim(W.shape[1])
    
        save_dir = self.save_folder + '/receptive_field_h'+str(layer)
                                
        image = Image.fromarray(
                    utils.tile_raster_images(X=W.T,
                    img_shape=(x, y),
                    tile_shape=tile_shape,
                    tile_spacing=(1, 1)))
        
        image.save((save_dir + '/epoch{}_receptive_fields.pdf').format(epoch))

    #%%
    def plot_active_fields(self,x,y,W,avg_data,epoch,layer):
        """ 
        Plots active fields. This is the receptive field that is elementwise
        multiplied with the average example image. I find this as a useful check
        that the receptive fields are learning unique features for each 
        example. 
        """

        n_v, n_h = W.shape
        tile_shape = utils.find_display_dim(n_h)
        
        save_dir = self.save_folder + '/active_field_h' + str(layer)
 
        for i in range(avg_data.shape[0]):
            cat_data = avg_data[i].reshape(n_v,1)
            cat_data = np.repeat(cat_data,n_h,axis=1)
            active = np.multiply(W,cat_data)
            image = Image.fromarray(
                        utils.tile_raster_images(X=active.T,
                        img_shape=(x, y),
                        tile_shape=tile_shape,
                        tile_spacing=(1, 1)))
            image.save((save_dir + '/epoch{}_active_fields{}.pdf').format(epoch,i))                           
    
    #%%
    def snapshots_mbs(self,epoch):
        """ 
        Plots pseudocost and recon per minibatch.
        This is mainly useful for debugging.        
        """
        if self.keep_snapshots:
            cost = self.all_cost/self.all_cost[0] # relative to start
            if self.train_class == 'unsuper':
                cost2 = self.all_recon/self.all_recon[0]
                cost_lbl = 'pseudo_cost' 
                cost2_lbl = 'recon'
            elif self.train_class == 'super':
                cost2 = self.all_errors/self.all_errors[0]
                cost_lbl = 'cost'
                cost2_lbl = 'errors'
        
            x = list(range(len(cost)))
            fig = plt.figure()
            plt.plot(x,cost,color='black',label=cost_lbl)
            plt.plot(x,cost2,color='blue',label=cost2_lbl)
            plt.xlabel('Minibatch')
            plt.ylabel('Normalized measures, all start at 1.0')
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            plt.title('Various cost measures')
            plt.grid(True)
        
            save_dir = self.save_folder + '/mb_details'
            savename = save_dir + '/epoch{}_mb_details.pdf'.format(epoch)                
            fig.savefig(savename, bbox_inches='tight')
            plt.close()         
    
    #%%
    def snapshots_final(self):
        """ 
        Separately plots all costs / measures vs all epochs   
        """            
     
        if self.keep_snapshots:     
         
            x = list(range(len(self.epoch_cost)))
                
            def plot_cost(x,y,color,title,savename):  
                fig = plt.figure()
                plt.plot(x,y,color=color)   
                plt.xlabel('Epoch')
                plt.title(title)
                plt.grid(True)
                fig.savefig(savename, bbox_inches='tight')
                plt.close()    
                
            def plot_detailed_cost(x, y, x_d, y_d,
                                   color, color_d, lbl, lbl_d,
                                   title, savename):  
                fig = plt.figure()
                plt.plot(x,y,color=color,label=lbl)   
                plt.scatter(x_d,y_d,color=color_d,label=lbl_d)
                plt.xlabel('Epoch')
                plt.title(title)
                plt.grid(True)
                plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
                fig.savefig(savename, bbox_inches='tight')
                plt.close()    

            def plot_ais(x,det_epoch,med,low,high,lbl,title,savename):
                fig = plt.figure()  
                low_err = np.array(med)-np.array(low)
                high_err = np.array(high)-np.array(med)
                data_err = np.array([low_err,high_err])
                plt.errorbar(det_epoch,med, yerr=data_err,color='blue',
                             fmt='o',ls='',label=lbl)
                plt.xlabel('Epoch')
                plt.xlim([-5,x[-1]+5]) # so you can see the start/end error bars
                plt.title(title)
                plt.grid(True)  
                plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)             
                fig.savefig(savename, bbox_inches='tight')
                plt.close()   
               

            # Norm cost plot           
            title = 'Avg norm cost'
            savename = self.save_folder + '/costs/norm.pdf' 
            plot_cost(x,self.epoch_norm_cost,'black',title,savename)          
              
            # Momentum plot
            title = 'Momentum for each epoch'
            savename = self.save_folder + '/costs/momentum.pdf' 
            plot_cost(x,self.epoch_mom,'black',title,savename)            
                   
            # Learning rate plot
            title = 'Learning rate for each epoch'
            savename = self.save_folder + '/costs/learning_rate.pdf' 
            plot_cost(x,self.epoch_lr,'black',title,savename)   


            if self.train_class == 'unsuper':
                # Pseudo Cost plot   
                title = 'Avg total pseudo cost of training data'
                savename = self.save_folder + '/costs/total_pseudo_cost.pdf'  
                plot_cost(x,self.epoch_cost,'black',title,savename)  
            
                # Pseudo like plot
                fig = plt.figure()
                plt.plot(x,self.epoch_pslike,color='green',label='example data') 
                #plt.scatter(self.detailed_epoch,self.detailed_pslike,
                #            label='all data' )
                N_avg = 10 # seems to need some smoothing
                if len(self.epoch_pslike) > 2*N_avg:
                    mv_avg = np.convolve(self.epoch_pslike, 
                                         np.ones((N_avg,))/N_avg, mode='valid')
                    plt.plot(x[(N_avg-1):],mv_avg,color='black',label='mv_avg_'+str(N_avg))
                plt.xlabel('Epoch')
                plt.title('Avg pseudolikelihood of training data')
                plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
                plt.grid(True)
                savename = self.save_folder + '/costs/pslike.pdf'                
                fig.savefig(savename, bbox_inches='tight')
                plt.close()
            
                # Reconstruction error plot
                title = 'Avg reconstruction error of training data'
                savename = self.save_folder + '/costs/recon.pdf'  
                plot_cost(x,self.epoch_recon,'red',title,savename)  
            
                # Average free energy difference
                title = 'FE valid minus FE train. Increasing implies overfitting.'
                savename = self.save_folder + '/costs/fe_diff.pdf' 
                plot_cost(x,self.epoch_fe_diff,'black',title,savename) 
        
                # Plot logz
                title = 'Log partition function with bounds'
                lbl = 'logZ detailed'
                savename = self.save_folder + '/costs/logz.pdf' 
                plot_ais(x,self.detailed_epoch,self.detailed_logz,
                         self.detailed_logz_10,self.detailed_logz_90,lbl,title,savename) 
            
                # Plot log pvalid
                title = 'Log prob valid with bounds'
                lbl = 'log(pv) detailed'
                savename = self.save_folder + '/costs/logpvalid.pdf' 
                plot_ais(x,self.detailed_epoch,self.detailed_logpvalid,
                         self.detailed_logpvalid_low,self.detailed_logpvalid_high,lbl,title,savename)
         
            elif self.train_class == 'super':
                # Cost plot   
                title = 'Avg total cost of training data'
                savename = self.save_folder + '/costs/total_cost.pdf' 
                plot_detailed_cost(x, self.epoch_cost, 
                                   self.detailed_epoch, self.detailed_valid_cost,
                                   'black', 'blue', 'train', 'valid',
                                    title, savename)                  
                
                title = 'Log10 of avg total cost of training data'
                savename = self.save_folder + '/costs/log_total_cost.pdf' 
                logcost = list(np.log10(self.epoch_cost))
                log_validcost = list(np.log10(self.detailed_valid_cost))
                plot_detailed_cost(x, logcost, 
                                   self.detailed_epoch, log_validcost,
                                   'black', 'blue', 'train', 'valid',
                                    title, savename)                  
                
                # error plot   
                title = 'Percent errors of training data'
                savename = self.save_folder + '/costs/total_errors.pdf'                                     
                train_errors = list(100*np.array(self.epoch_errors)) 
                valid_errors = list(100*np.array(self.detailed_valid_errors))                 
                plot_detailed_cost(x, train_errors, 
                                   self.detailed_epoch, valid_errors,
                                   'black', 'blue', 'train', 'valid',
                                    title, savename)

                title = 'Log10 percent errors of training data'
                savename = self.save_folder + '/costs/log_total_errors.pdf'                                     
                train_errors = list(np.log10(100*np.array(self.epoch_errors)))
                valid_errors = list(np.log10(100*np.array(self.detailed_valid_errors)))                 
                plot_detailed_cost(x, train_errors, 
                                   self.detailed_epoch, valid_errors,
                                   'black', 'blue', 'train', 'valid',
                                    title, savename)
