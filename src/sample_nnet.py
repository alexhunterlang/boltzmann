"""
Sampling Neural Networks in Theano

Original Code Source: 
    [1] http://deeplearning.net/tutorial/rbm.html
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

For now, I usually skip 2000 gibbs steps, but this should be systematically
studied for convergence rate

"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano
import my_data
import utils
import keras.backend as K

#%%
def sample_nnet(nnet_obj,data,n_each,n_samples,plot_every):
    """ Samples from Neural Network 
    
    :param nnet_obj: neural network object to sample
    :param data: my_data.Data object    
    :param n_each: number of each example to initialize gibbs chain   
    :param n_samples: number of samples per chain
    :param plot_every: how long to wait between sampling chain
            
    """     
    assert type(data) is my_data.Data   
    
    # pick random test examples, with which to initialize the persistent chain
    index = data.test.get_index_examples(n_each=n_each) 
    n_chains = index.shape[0]

    init_v = np.array(data.test.data.get_value(borrow=False)[index],
                            dtype=K.floatx())
    #init_v = np.random.uniform(size=(n_chains,784))
    #print(init_v.shape)
    prob_ls = nnet_obj.propup(init_v, IS_dropout = False)
    prob_ls = [init_v]+prob_ls

    chain_ls = []
    for i,p in enumerate(prob_ls):
        if i>0:
            p = p.eval()
        chain = theano.shared(p, name='chain'+str(i), borrow=True)
        chain_ls.append(chain)                            
          

    # define one step of Gibbs sampling 
    # function that does `plot_every` steps before returning the
    # sample for plotting                  
    output_ls = [None]*len(chain_ls) + chain_ls + [None]*len(chain_ls)                        
    
    scan_output, updates = theano.scan(nnet_obj.gibbs_even_odd_even, 
                                        outputs_info = output_ls, 
                                        n_steps=plot_every,
                                        name = 'scan_sample')                           
    
    # add to updates the shared variable that takes care of our persistent
    # chain :.
    prob_out = scan_output[len(chain_ls):2*len(chain_ls)]
    for ch, p in zip(chain_ls, prob_out):
        updates.update({ch : p[-1]})
        #updates[ch] = p[-1]

    prob_v = prob_out[0]
    #prob_v = scan_output[len(chain_ls)]

    # construct the function that implements our persistent chain.
    sample_fxn = theano.function([],prob_v[-1],
                                updates=updates, name='sample_fxn')

    # create a space to store the image for plotting ( we need to leave
    # room for the tile_spacing as well)
    ts = 1 # tile spacing
    x = data.x
    y = data.y
    xx = x+ts
    yy = y+ts    
    image_data = np.zeros((xx*(n_samples+2)+1, yy*n_chains-1), dtype='uint8')              
                
    image_data[0:x,:] = utils.tile_raster_images(
                            X=init_v, img_shape=(x,y),
                            tile_shape=(1, n_chains), tile_spacing=(ts, ts))
    for idx in range(n_samples):
        # generate `plot_every` intermediate samples that we discard,
        # because successive samples in the chain are too correlated
        # I left a blank row between original images and gibbs samples
        vis_prob = sample_fxn()
        image_data[xx*(idx+2) : xx*(idx+2) + x, :] =\
            utils.tile_raster_images(X=vis_prob, img_shape=(x, y),
                tile_shape=(1, n_chains), tile_spacing=(ts, ts))

    return image_data