#%%
from __future__ import division

import numpy as np
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

np.random.seed(1234) # seed random number generator
srng_seed = np.random.randint(2**30)

from keras.optimizers import SGD

from models import SingleLayerUnsupervised
from rbm import RBM

# configuration
input_dim = 100
hidden_dim = 50
batch_size = 100
nb_epoch = 2
lr = 0.0001  # small learning rate for GB-RBM

#%%
def main():
    # generate dummy dataset
    nframes = 10000
    dataset = np.random.normal(loc=np.zeros(input_dim), scale=np.ones(input_dim), size=(nframes, input_dim))

    # split into train and test portion
    ntest   = 1000
    X_train = dataset[:-ntest :]     # all but last 1000 samples for training
    X_test  = dataset[-ntest:, :]    # last 1000 samples for testing
    assert X_train.shape[0] >= X_test.shape[0], 'Train set should be at least size of test set!'

    # setup model structure
    rbm = RBM(input_dim=input_dim, hidden_dim=hidden_dim)
    train_model = SingleLayerUnsupervised()
    train_model.add(rbm)
    
    opt = SGD(lr, 0.5, decay=0.0, nesterov=False)

    contrastive_divergence = rbm.contrastive_divergence_loss(nb_gibbs_steps=1)

    # compile theano graph
    train_model.compile(optimizer=opt, loss=contrastive_divergence)

    train_model.fit(X_train, batch_size, nb_epoch, verbose=1, shuffle=False)

#%%
if __name__ == '__main__':
    main()
