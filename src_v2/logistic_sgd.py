"""
Logistic Regression by Stochastic Gradient Descent in Theano

Original Code Source: 
    [1] http://deeplearning.net/tutorial/logreg.html
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

This code is mainly optimized for speed, not for convergence to the smallest
possible accuracy
"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import timeit
import numpy as np
import theano
import theano.tensor as T
import my_data
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams 
from layers import InputLayer, OutputLayer

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

#%%
class LogisticRegression(object):
    """
    Multi-class Logistic Regression Class

    The logistic regression is fully described by a weight matrix W
    and bias vector b. Classification is done by projecting data
    points onto a set of hyperplanes, the distance to which is used to
    determine a class membership probability.
    """

    def __init__(self, data_input, n_in, n_out, mbs):
        
        # create a number generator
        self.np_rng = np.random.RandomState()
        self.theano_rng = RandomStreams(self.np_rng.randint(2 ** 30))
        
        self.layer_ls = []        
        
        layer = InputLayer(data_input, n_in, mbs, self.np_rng, self.theano_rng, 
                           prob_drop = 0.0)
        self.layer_ls.append(layer)                 
                 
                 
        W = np.zeros((n_in,n_out), dtype=FLOAT)
        b = np.zeros((n_out,), dtype=FLOAT)
        self.out_layer = OutputLayer(layer, n_out, index = 1, 
                                     L1 = 0.0, L2 = 0.0, W = W, b = b)
        self.layer_ls.append(self.out_layer)
        
        self.W = self.out_layer.W
        self.b = self.out_layer.b
        
    def negative_log_likelihood(self,data_lbl):
        
        nll = self.out_layer.negative_log_likelihood(data_lbl,
                                                     IS_dropout = False)        
        
        return nll
        
    def errors(self,data_lbl):
        return self.out_layer.errors(data_lbl)
    
    def update_best(self):
        self.out_layer.update_best()
        
    def get_best_pred(self,data_input):
        return self.out_layer.get_best_pred(data_input)                
                 
#%%
def sgd_optimization(data, lr=0.3, max_epochs=500, mbs=1000, verbose=False):
    """
    Demonstrate stochastic gradient descent optimization of a log-linear
    model

    Data = input dataset
    
    lr = learning rate. 0.3 works for MNIST
    
    max_epochs = final cutoff epoch. Only need around 50 epochs for MNIST
    
    mbs = mini batch size. 1000 works for MNIST
    
    verbose = Should one print training details to screen?

    """
    
    # TODO:
    # 1. probably should return warning if reached max_epochs, even on
    #       non-verbose mode    
    
    assert type(data) is my_data.Data    
    
    # Early stopping parameters
    patience = 25 # number of epochs for sure
    patience_increase = 10 # number of epochs to increase by 
    improvement_threshold = 0.99  # a relative improvement of this much is
                                  # considered significant    
    
    start_tot_time = timeit.default_timer()

    ###### Build the model #######

    # allocate symbolic variables for the data
    index = T.vector(name='index', dtype=INT)  # index to a [mini]batch

    # generate symbolic variables for input (x and y represent a
    # minibatch)
    # data, presented as rasterized images
    data_input = T.matrix(name='data_input', dtype=FLOAT)  
    # labels, presented as 1D vector of [int] labels
    data_lbl = T.vector(name='data_lbl', dtype=INT)  
    
    # construct the logistic regression class
    logreg = LogisticRegression(data_input = data_input, 
                                    n_in = data.n_input, n_out = data.n_cat,
                                    mbs = mbs)

    # the cost we minimize during training is the negative log likelihood of
    # the model in symbolic format
    cost = logreg.negative_log_likelihood(data_lbl)

    # compiling a Theano function that computes the mistakes that are made by
    # the model on a minibatch
    validate_model = theano.function(inputs=[index],
                                    outputs=logreg.errors(data_lbl),
                                    givens={data_input: data.valid.data[index],
                                            data_lbl: data.valid.lbl[index]})

    # compute the gradient of cost with respect to theta = (W,b)
    g_W = T.grad(cost=cost, wrt=logreg.W)
    g_b = T.grad(cost=cost, wrt=logreg.b)

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs.
    updates = [(logreg.W, logreg.W - lr * g_W),
               (logreg.b, logreg.b - lr * g_b)]

    # compiling a Theano function `train_model` that returns the cost, but in
    # the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(inputs=[index],
                                  outputs=cost,
                                  updates=updates,
                                    givens={data_input: data.train.data[index],
                                            data_lbl: data.train.lbl[index]})


    ####### TRAIN MODEL ###########

    best_valid_error = np.inf
    done_looping = False
    start_epoch_time = timeit.default_timer()   
    
    epoch = 0
    while (epoch < max_epochs) and (not done_looping):        
        # need to randomize order of training data
        data.train.randomize_mb()
        for current_index in data.train:    
            _ = train_model(current_index)
            
        validation_errors = []    
        for current_index in data.valid:
            validation_errors.append(validate_model(current_index))
        valid_avg_errors = data.valid.avg_over_mbs(validation_errors)
            
        if verbose:
            print('epoch {}, validation error {:0.2f}%'.format(
                            epoch,valid_avg_errors*100.0))

        # Check if best validation score
        # and needs to be a significant improvement
        if valid_avg_errors < best_valid_error * improvement_threshold:
            patience = max(patience, epoch + patience_increase)
            best_valid_error = valid_avg_errors
            logreg.update_best()                    
            
        if epoch >= patience:
            done_looping = True
            break
        else:
            epoch += 1

    train_predict = logreg.get_best_pred(data.train.data).eval()
    valid_predict = logreg.get_best_pred(data.valid.data).eval()
    test_predict = logreg.get_best_pred(data.test.data).eval()

    end_time = timeit.default_timer()

    if verbose:
        tot_time = end_time-start_tot_time
        print('The code run for {} epochs, with {:0.2f} epochs/sec'.format(
            epoch, 1. * epoch / (end_time - start_epoch_time)))
        print('Total run time of {:0.2f} seconds'.format(tot_time)) 
            
    return train_predict, valid_predict, test_predict


#%%
if __name__ == '__main__':
    lr=0.3
    max_epochs=50
    mbs=1000
    data = my_data.Data('MNIST',mbs=mbs)
    train_predict,valid_predict,test_predict = \
                    sgd_optimization(data,lr,max_epochs,mbs)
