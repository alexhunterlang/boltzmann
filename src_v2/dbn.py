""" 
Deep Belief Network (DBN)

Written by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

This implements a DBN by stacking a series of RBMs on top of each other. 
The first RBM provides output data that is then used by the next RBM to 
train.

Currently just a wrapper of rbm code to pretrain a DBN for a DBM

Timing on GPU for pretraining only
MNIST_binary, n_h1=nh2=500, pCDk=5, n_epochs=100 takes 26.4 min
"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import timeit
import numpy as np
import my_data
import utils
import train_nnet
import dbm
import theano
import theano.tensor as T

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

#%% DBN
class DBN(object):
    def __init__(self,n_h_ls):
        
        self.n_h_ls = n_h_ls
        self.n_layers = len(n_h_ls)
        self.rbm_list = []
  
    #%%
    def prep_rbm_data(self,data,layer):
        """ Makes new data object based on output of rbm """
        
        network_dict = {'nnet': self.rbm_list[layer-1],
                        'data': data}            
        next_data = my_data.Data('neural_network', mbs=data.mbs,
                                 load_network_dict=network_dict)
                                 
        return next_data
  
    #%%
    def output_nnet(self):
        # Get weights
        W_list = []
        for rbm_obj in self.rbm_list:
            W_list.append(rbm_obj.synapses_ls[0].output_W())
            
        # Get biases
        bias_list = []
        for i in range(self.n_layers):
            b0 = self.rbm_list[i].layers_ls[0].output_b()
            bias_list.append(b0)
        bias_list.append(self.rbm_list[-1].layers_ls[1].output_b())
        
        # Save arrays to a dictionary
        # This successfully preserves names for each array
        d_out = {}
        for i,b in enumerate(bias_list):
            key = 'b'+str(i)
            d_out[key] = b
        for i,W in enumerate(W_list):
            key = 'W'+str(i)+str(i+1)
            d_out[key] = W
            
        return d_out
  
    #%%
    def save_nnet(self,save_folder,filename):    
        """ save network to a file """

        d_out = self.output_nnet()        
        
        # save output
        outfile = save_folder + '/' + filename
        np.savez_compressed(outfile, dbn_dict = d_out)
        
    #%%
    def pretrain_DBN(self, dataset, data, n_epochs, n_epochs_early,
                     mbs, lr, lr_decay, mom_early, mom_late,
                     L1, L2, prob_drop, k, IS_mf, persistent, network_to_load, 
                     save_folder, keep_snapshots, save_all, IS_dbm_pre):                                    
        """ Implements unsupervised pretraining """
        # Pretraining of contrastive divergence
        # No feedback with data labels
        # Usually no early stopping in this stage

        print("Starting pretraining for DBN")
        for i in range(self.n_layers):
            print("\n========================================\n")
            print("Pretraining RBM layer {:d}\n\n".format(i)) 
            
            # This passes data to the next layer
            if i>0:
                data = self.prep_rbm_data(data,i)
                
            # adjusts rbm inputs so pretraining matches desired dbm weights
            if i==0:    
                dbm_pre = 'bottom'
            elif i+1==self.n_layers:
                dbm_pre = 'top'
            else:
                dbm_pre = 'middle'
                
            n_h = self.n_h_ls[i]    
            rbm_folder = save_folder + '/rbm{}'.format(i)            
            
            data_input = T.matrix('data_input',dtype=FLOAT)  # rasterized images

            rbm_obj = dbm.DBM(data_input, data, [n_h], mbs, k , L1, L2, IS_mf,
                          persistent, prob_drop, None, None, network_to_load,
                          dbm_pre)                        
                          
            self.rbm_list.append(rbm_obj)              

            train_nnet.train_nnet(data_input, data, rbm_obj, n_epochs, 
                                  n_epochs_early, lr, 0.0,  mom_early, 
                                  mom_late, rbm_folder,
                                  keep_snapshots, save_all)            

#%%
def main(dataset, n_epochs, n_epochs_early, n_h_ls, mbs,
         lr, lr_decay, mom_early, mom_late, L1, L2, prob_drop, 
         k, IS_mf, persistent, network_to_load, 
         save_folder, keep_snapshots, save_all, pre_dbm):   
    
    start_tot_time = timeit.default_timer()

    # Obtains data object
    data = my_data.Data(dataset,mbs)   
        
    # makes master folder for dbn
    utils.safe_make_folders(save_folder)    

    # make the dbn    
    dbn_obj = DBN(n_h_ls)

    # Perform the pretraining
    dbn_obj.pretrain_DBN(dataset, data, n_epochs, n_epochs_early,
                     mbs, lr, lr_decay, mom_early, mom_late,
                     L1, L2, prob_drop, k, IS_mf, persistent, network_to_load, 
                     save_folder, keep_snapshots, save_all, pre_dbm)

    dbn_obj.save_nnet(save_folder,'model_final_DBN')
    
    tot_time = timeit.default_timer() - start_tot_time
    print('Total DBN runtime of {:0.3f} minutes'.format(tot_time / 60.0))
    
    return dbn_obj

#%%
if __name__ == '__main__':
    """ Pretrains a DBN """
    # The dataset that you choose or the given input will automatically 
    # set the dimension of the visible layer. The hidden layer size is a 
    # free parameter. Nesterov momentum is implemented.
    # If given no input weights / bias, implements random
    # orthogonal weight matrices. 
    # Regularization only applies to weights not biases.

    # assumed folder structure:
    # parent folder path = pf
    # pf/src - where this code and others live
    # pf/data - where datasets are stored
    # pf/results - where results will be stored
    
    # Parameters to input
    # If don't want some option, set to 0.0 unless noted otherwise
    # number in {} is recommendation
    
    dataset = 'MNIST_binary' # see my_data for options 
    n_epochs = 100 # {100} for pretraining, {500} for training
    n_epochs_early = 10 # {10} mom_early ramping up and no decay 
    n_h_ls = [500,1000]
    mbs = 100 # {100} size of minibatch
    lr = 0.001 # {0.005} # Bengio notes best lr is around 1/2 of diverging lr 
    lr_decay = 9.0/490 #{9.0/490} later epochs only. See Note 1 below    
    mom_early = 0.5 # {0.5}, up to n_epochs_early, ramps up exponentially
    mom_late = 0.9 # {0.9}, after n_epochs_early
    L1 = 0.0 # L1 penalty, {1e-5} see Note 2 below
    L2 = 1e-5 # L2 penalty, {1e-5} see Note 2 below
    prob_drop = 0.0
    k = 5 # Number of CDk steps, {1} for pretraining, {5} for unsuper
    IS_mf = True # {True}, mean field or TAP
    persistent = True # Standard CD vs persistent CD, {True} for unsuper
    network_to_load = None # filename of pretrained network or None
    save_folder = '../results/test' # folder to save results to
    keep_snapshots=True # unless debugging, print out lots of details 
    save_all = False # normal only save model when recording statistics

    # Note 1:
    # true_lr = lr/(1+lr_decay*(epoch-n_early))

    # Note 2
    # L1/L2 = 1e-3 seems too high, L1/L2=0.0 is also not optimal
    # But any combination of L1,L2 from [0.0, 1e-4, 1e-5] is similar

    #################################################

    dbn_obj = main(dataset, n_epochs, n_epochs_early, n_h_ls, 
                         lr, lr_decay, mom_early, mom_late, L1, L2, prob_drop, 
                         k, IS_mf, persistent, network_to_load, 
                         save_folder, keep_snapshots, save_all)