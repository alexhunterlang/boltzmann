""" 
Annealed Importance Sampling of an RBM

Original Code Source: 
    https://github.com/lisa-lab/pylearn2/blob/master/pylearn2/rbm_tools.py
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

Useful References:
[1] Neal (2001) Annealed importance sampling
    https://link.springer.com/article/10.1023/A:1008923215028
[2] Salakhutdinov, Murray (2008) On the quantitative analysis of DBNs
    http://www.cs.toronto.edu/~rsalakhu/papers/dbn_ais.pdf
[3] http://www.cs.toronto.edu/~rsalakhu/rbm_ais.html

TODO: eventually have option to return std as calculated in [3]. But I found
    std to be misleading for fast estimates, so I used percentiles

"""
#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano
import theano.tensor as T
import dbm
import my_data
import timeit
from utils import binomial_sample
from theano.tensor.nnet import sigmoid

FLOAT = theano.config.floatX # should be float32 for GPU
INT = str('int32') # again 32 bits for GPU

#%% AIS object
class AIS(object):
    """ 
    annealed importance sampling AIS 

    n_betas = 1.5e4 seems like enough (papers use 1e4 to 2e4)
    increase n_runs to decrease variance    
    
    for quick rbm estimate, n_runs=100,n_betas=1e3 is fast and has reasonable error  
   
    GPU Timing (updates only, not fxn compile)
    RBM with n_v = 784, n_h = 500
    n_runs = 1e2, n_betas = 1e3 : 0.66 sec
    n_runs = 1e3, n_betas = 1.5e4 : 35.0 sec
    DBM with n_v = 784, n_h1 = 500, n_h2 = 500
    n_runs = 1e2, n_betas = 1e3 : 1.1 sec
    n_runs = 1e3, n_betas = 1.5e4 : 59.4 sec
    
    """
    def __init__(self,  nnet_obj, data_obj, 
                 n_runs = 100, n_betas = 1e3):
        """
        nnet_obj : neural network object (such as RBM)
        data_obj : data object (such as MNIST)
        np_rng : numpy random number generator
        theano_rng: theano random number generator 
        n_runs : number of independent AIS runs
        n_betas : number of betas (will be choosen on log scale)   
        """
        
        assert type(data_obj) is my_data.Data      
        
        self.n_betas = np.int(n_betas)
        self.n_runs = np.int(n_runs)                     
        self.n_v = data_obj.n_input
        self.n_h_ls = nnet_obj.n_all[1:]
        self.n_all = nnet_obj.n_all
                
        self.weight_ls = nnet_obj.weight_final_ls
        self.bias_ls = nnet_obj.bias_ls        
        self.all_layers = nnet_obj.layers_ls
        self.even_layers = self.all_layers[0::2]
        self.odd_layers = self.all_layers[1::2]
        self.odd_bias = self.bias_ls[1::2]
        
        self.np_rng = nnet_obj.np_rng
        self.theano_rng = nnet_obj.theano_rng
        
        self.nonlin = sigmoid

        # Create logarithmically increasing beta
        min_beta = 1e-3
        init_betas = np.log10(np.linspace(min_beta,1,self.n_betas))
        init_betas = (init_betas - np.log10(min_beta))/(-np.log10(min_beta))
        self.betas = theano.shared(value=np.array(init_betas,dtype=FLOAT),
                                   name='betas',borrow=True)

    #%% 
    def run(self):   
        """ code to actual implement AIS calc """  
        
        log_w = T.zeros(self.n_runs, dtype=FLOAT)
        index = T.cast(0.0, dtype = FLOAT)
        
        state_ls = []
        for b in self.odd_bias:    
            b_exp = T.reshape(b,(1,b.size.eval().item(0)))
            prob = T.tile(1.0/(1.0 + T.exp(-b_exp)), (self.n_runs, 1))
            init_state = binomial_sample(self.theano_rng,prob)
            state_ls.append(init_state)
            
        output_ls = [log_w] + state_ls + [index]
        
        scan_output, updates = theano.scan(self.update_fxn,
                                           outputs_info = output_ls,
                                           name = 'scan_ais',
                                           n_steps = self.n_betas-1)

        log_w_ls = scan_output[0]
                                     
        ais_fxn = theano.function([], log_w_ls[-1], 
                                  updates = updates, name = 'ais_fxn')
        
        log_w_final = ais_fxn()
    
        # Calculate final results
        logz, logz_90, logz_10 = self.estimate_from_weights(log_w_final)
        
        return logz, logz_90, logz_10

    #%%
    def update_fxn(self, *args):
        
        input_ls = list(args)        
        
        log_w = input_ls[0]    
        index = input_ls[-1]
        state_ls = input_ls[1:-1]
        
        # DBM with 2 hidden layers is illustrated here

        # fe as fxn of beta_fe
        #h1_term = beta_fe * T.dot(self.state, self.b1)
        #h2_act = beta_fe * (T.dot(self.state, self.W12) + self.b2) 
        #h2_term = T.sum(T.log(1 + T.exp(h2_act)),axis=1)
        #v_act = beta_fe * (T.dot(self.state, self.W01.T) + self.b0) 
        #v_term = T.sum(T.log(1 + T.exp(v_act)),axis=1)        
        # fe_out = -h1_term - h2_term - v_term

        # fe_a0 = fe[(1-beta0),odd bias only]
        # fe_b0 = fe[beta0,all]
        # fe_0 = fe_a0 + fe_b0

        # beta1 is same idea, just different temp

        # change to log_w is fe_beta0 - fe_beta1

        # BUT there is a bunch of cancellation of terms
        # Only thing that remains is calculated below

        # Additionally calculations are potentially saved (maybe theano catches it?)
        # by doing update and sampling at once

        prob_ls = [None]+state_ls+[None] 

        beta0 = self.betas[T.cast(index,dtype=INT)] 
        beta1 = self.betas[T.cast(index+1,dtype=INT)]               
        samples_even = []

        # This updates change in log_w as well as first part of sampling
        for i,layer in enumerate(self.even_layers):
            z = layer.get_input(input_up = prob_ls[i],
                                     input_self = None, 
                                     input_down = prob_ls[i+1],
                                     IS_mf = True,
                                     direction = None,
                                     IS_dropout = False)                                     
                                     
            # change in log_w                        
            b0_term = - T.sum(T.log(1 + T.exp(beta0*z)),axis=1)
            b1_term = - T.sum(T.log(1 + T.exp(beta1*z)),axis=1)
            log_w += b0_term - b1_term
                
            # sampling
            sample = binomial_sample(self.theano_rng, self.nonlin(beta1*z))
            samples_even.append(sample)
            
            
        # this updates the samples for the next state
        samples_even.append(None)
        samples_odd = []
        i=0   
        for b, layer in zip(self.odd_bias,self.odd_layers):
            z = layer.get_input(input_up = samples_even[i],
                                     input_self = None, 
                                     input_down = samples_even[i+1],
                                     IS_mf = True,
                                     direction = None,
                                     IS_dropout = False)
            prob = self.nonlin((1-beta1)*b + beta1*z)
            sample = binomial_sample(self.theano_rng, prob)
            samples_odd.append(sample)
            i += 1   

        index += 1

        output_ls = [log_w] + samples_odd + [T.cast(index,dtype=FLOAT)]

        return  output_ls

    #%% Estimate mean and variance
    def estimate_from_weights(self,log_w_final):
        """
        Estimates mean and variance of log(Zb/Za)   
        """
        
        log_za = np.sum(self.n_all[0::2])*np.log(2)
        for b in self.odd_bias:
            log_za += np.sum(np.log(1 + np.exp(b.get_value())))              
    
        logz = np.median(log_w_final) + log_za
        logz_90 = np.percentile(log_w_final,90) + log_za
        logz_10 = np.percentile(log_w_final,10) + log_za

        return logz, logz_90, logz_10
        
#%%
if __name__ == '__main__':
    """ Implements AIS for an RBM"""

    data_obj = my_data.Data('MNIST_binary')
    
    data_input = T.matrix(dtype=FLOAT)
    n_h_ls = [500,500]    
        
    dbm_obj = dbm.DBM(data_input, data_obj, n_h_ls, 
                 mbs = 100, k = 5, L1 = 0.0, L2 = 0.0, IS_mf = True,
                 IS_persist = True, prob_drop = 0.0, np_rng = None, 
                 theano_rng = None, network_to_load=None)

    start_time = timeit.default_timer()

    ais_obj = AIS(dbm_obj, data_obj, n_runs = 1000, n_betas = 15000)  

    run_time = timeit.default_timer()
    
    logz, logz_90, logz_10 = ais_obj.run()    

    run_time = timeit.default_timer()-run_time
    
    total_time = timeit.default_timer() - start_time
    
    print('Update time took {:0.3f} minutes'.format(run_time/60))
    print('Total runtime took {:0.3f} minutes'.format(total_time/60))