"""
MLP

"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano
import theano.tensor as T
from theano.tensor.nnet import sigmoid
import os
import my_data
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams 
import warnings
import train_nnet
import utils
from collections import OrderedDict
from layers import InputLayer, HiddenLayer, OutputLayer

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

msg = "numpy.ndarray size changed, may indicate binary incompatibility"
# can safely ignore this warning
# see https://groups.google.com/forum/#!topic/theano-users/A__NVIBYMxA
warnings.filterwarnings("ignore", message=msg)

#%%        
#%%
class MLP(object):

    def __init__(self, data_input, data_lbl, data, n_h_ls, mbs, L1, L2, 
                 prob_drop_ls = None, network_to_load = None):
       
        self.train_class = 'super'       
       
        if network_to_load is not None:
            raise NotImplementedError()
       
        assert type(data) is my_data.Data
        assert len(n_h_ls)>0       
       
        self.data_input = data_input
        self.data_lbl = data_lbl
        self.mbs = mbs
        if prob_drop_ls is None:
            self.prob_drop_ls = (len(n_h_ls)+1)*[0]
        else:
            assert len(prob_drop_ls) == (len(n_h_ls)+1)
            self.prob_drop_ls = prob_drop_ls
        self.IS_dropout = np.any(np.array(self.prob_drop_ls)>0)
        self.L1 = L1
        self.L2 = L2
       
        # create a number generator
        self.np_rng = np.random.RandomState()
        self.theano_rng = RandomStreams(self.np_rng.randint(2 ** 30))
       
        self.n_in = data.n_input
        self.n_out = data.n_cat
        self.n_h_ls = n_h_ls        
        self.n_ls = [self.n_in] + self.n_h_ls + [self.n_out]
        self.n_layers = len(self.n_ls)
        
        self.IS_nest = True        
        
        self.layers_ls = []
        
        # input layer
        layer = InputLayer(self.data_input, self.n_in, self.mbs,
                           self.np_rng, self.theano_rng, self.prob_drop_ls[0])
        self.layers_ls.append(layer)
        
        # hidden layers
        for i in range(1,len(self.n_ls)-1):
            prob_drop = self.prob_drop_ls[i-1]
            layer = HiddenLayer(layer, self.n_ls[i], i, self.L1, self.L2,  
                                prob_drop = prob_drop,
                                nonlin = sigmoid, W = None, b = None)
            self.layers_ls.append(layer)
        
        # output layer
        i = self.n_layers - 1
        layer = OutputLayer(layer, self.n_ls[i], i, 
                            self.L1, self.L2, W = None, b = None)                            
        self.layers_ls.append(layer)
        
        self.cost   = layer.negative_log_likelihood
        self.errors = layer.errors

        
        # the parameters of the model are the parameters of the two layer it is
        # made out of
        
        self.weight_ls = []
        self.bias_ls = []
        self.vel_W_ls = []
        self.vel_b_ls = []
        for layer in self.layers_ls[1:]:
            self.weight_ls.append(layer.W)
            self.bias_ls.append(layer.b)
            self.vel_W_ls.append(layer.vel_W)
            self.vel_b_ls.append(layer.vel_b)           
            
        self.params = self.weight_ls + self.bias_ls
        self.vel_ls = self.vel_W_ls + self.vel_b_ls

    #%%            
    def update_best(self, epoch):
        ''' Records best parameters '''
        self.best_epoch = epoch
        for layer in self.layers_ls[1:]:
            layer.update_best()

    #%%
    def get_norm_cost(self, want_np = False):
        ''' Returns weight costs '''
        if want_np:
            cost = 0.0
        else:
            cost = T.cast(0.0,dtype=FLOAT)    
            
        for layer in self.layers_ls[1:]:
            cost += layer.get_cost(want_np)
        
        return cost

    #%%
    def update_dropout(self, updates):
        
        for layer in self.layers_ls:
            updates = layer.update_dropout(updates)
            
        return updates

    #%%
    def propup(self, vis, IS_dropout = False):
        """ Pass data up through network"""
        
        output_ls = []  
        
        output = self.layers_ls[0].get_output(vis, IS_dropout)
        
        for layer in self.layers_ls[1:]:
            output = layer.get_output(output, IS_dropout)
            output_ls.append(output)               
                
        return output_ls  

    #%%
    def training_update(self, lr, mom):

        cost = self.cost(self.data_lbl, self.IS_dropout)
        cost += self.get_norm_cost()
        
        error = self.errors(self.data_lbl)

        grads = T.grad(cost, self.params)
        
        updates = OrderedDict()    
        
        for g, param, vel in zip(grads, self.params, self.vel_ls):
            new_vel = mom*vel - lr*g
            updates[vel] = new_vel
            if self.IS_nest:
                # Nesterov - ref [4]
                updates[param] = param + mom*new_vel - lr*g 
            else:
                # standard
                updates[param] = param + new_vel 
         
        # update masks for next minibatch
        if self.IS_dropout:
            updates = self.update_dropout(updates)        
                    
        return cost, error, updates

    #%%
    def output_act(self, vis):
        """
        Given input visible data, returns output activations as numpy array
        """        
        output_ls = self.propup(vis, IS_dropout = False)
        
        output_np_ls = []
        for output in output_ls:
            output_np_ls.append(output.eval())
        
        return output_np_ls

        #%%
    def output_nnet(self):
        # Save arrays to a dictionary
        # This successfully preserves names for each array
                
        d_out = {}    
        for i,layer in enumerate(self.layers_ls[1:]):
            key = 'W'+str(i)+str(i+1)
            d_out[key] = layer.output_W()
        for i,layer in enumerate(self.layers_ls[1:]):
            key = 'b'+str(i+1)
            d_out[key] = layer.output_b()
            
        return d_out

    #%%
    def save_nnet(self,save_folder,filename):
        """ save network to a file """

        d_out = self.output_nnet()        

        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
        outfile = save_folder + '/' + filename
        np.savez_compressed(outfile, dbm_dict = d_out)


#%%
def main(dataset, n_epochs, n_epochs_early, n_h_ls, mbs,
         lr, lr_decay, mom_early, mom_late, L1, L2, prob_drop_ls,
         network_to_load, save_folder, keep_snapshots, save_all):
    """
    :param dataset: name of dataset (MNIST, RBM, etc see my_data.py)

    :param n_epochs: number of epochs used for training
    
    :param n_epochs_early: number of epochs with mom_early and no lr decay
    
    :param n_h_ls: number of hidden units per layer

    :param mbs: Size of minibatch

    :param lr: learning rate used for training the neural network
    
    :param lr_decay: late_lr = lr/(1+lr_decay*(epoch-n_epochs_early))

    :param mom_early: coefficient of Nesterov momentum for early epochs 
    
    :param mom_late: coefficient of Nesterov momentum for late epochs
    
    :param L1: L1 norm penalty
    
    :param L2: L2 norm penalty
    
    :param k: Number of steps for pCDk or CDk    
    
    :param IS_mf: Mean field or TAP
    
    :param persistent: True for pCDk, False for CDk
    
    :param pretrain: If true, pretrains with a DBN

    :param network_to_load: Network object to load
    
    :param save_folder: Path where results are saved
    
    :param keep_snapshots: whether to record all statistical details
    
    :param save_all: whether to save model at every epoch or only when 
                        recording statistical details
    """
    
    # Save parameters used in simulation
    filename = save_folder + '/parameters.txt'
    param_dict = {'nnet_type' : 'mlp', 'dataset' : dataset,
                  'n_epochs' : n_epochs, 'n_epochs_early' : n_epochs_early,
                  'n_h_ls' : n_h_ls, 'mbs' : mbs, 
                  'lr' : lr, 'lr_decay' : lr_decay, 'mom_early' : mom_early, 
                  'mom_late' : mom_late, 'L1' : L1, 'L2' : L2,
                  'prob_drop_ls' : prob_drop_ls, 'network_to_load' : network_to_load}
    utils.save_dict(filename,param_dict)
    
    # Obtains data object
    data = my_data.Data(dataset,mbs)  

    print('Training MLP\n\n')    

    data_input = T.matrix('data_input',dtype=FLOAT)  # rasterized images
    data_lbl = T.vector('data_lbl',dtype=INT)

    mlp_obj = MLP(data_input, data_lbl, data, n_h_ls, mbs, L1, L2, 
                 prob_drop_ls, network_to_load)

    train_nnet.train_nnet(data_input, data, mlp_obj, n_epochs, 
                              n_epochs_early, lr, lr_decay,  mom_early, 
                              mom_late, save_folder, keep_snapshots, 
                              save_all, data_lbl)
                                    
    return mlp_obj, data

#%%
if __name__ == '__main__':
    """ Trains a DBM """
    # The dataset that you choose or the given input will automatically 
    # set the dimension of the visible layer. The hidden layer size is a 
    # free parameter. Nesterov momentum is implemented.
    # If given no input weights / bias, implements random
    # orthogonal weight matrices. 
    # Regularization only applies to weights not biases.

    # assumed folder structure:
    # parent folder path = pf
    # pf/src - where this code and others live
    # pf/data - where datasets are stored
    # pf/results - where results will be stored
    
    # Parameters to input
    # If don't want some option, set to 0.0 or None, see below
    # number in {} is recommendation
    
    dataset = 'MNIST_binary' # see my_data for options 
    n_epochs = 1 # {100} for pretraining, {500} for training
    n_epochs_early = 10 # {10} mom_early ramping up and no decay 
    n_h_ls = [500,1000]
    mbs = 100 # {100} size of minibatch
    lr = 0.005 # {0.005} # Bengio notes best lr is around 1/2 of diverging lr 
    lr_decay = 1.0/490 #{9.0/490} later epochs only. See Note 1 below    
    mom_early = 0.5 # {0.5}, up to n_epochs_early, ramps up exponentially
    mom_late = 0.9 # {0.9}, after n_epochs_early
    L1 = 5e-5 # L1 penalty, {1e-5} see Note 2 below
    L2 = 5e-5 # L2 penalty, {1e-5} see Note 2 below
    prob_drop_ls = [0.2,0.5,0.5]
    network_to_load = None # filename of pretrained network or None
    save_folder = '../results/test_mlp' # folder to save results to
    keep_snapshots = True # unless debugging, print out lots of details 
    save_all = False # normal only save model when recording statistics

    assert (prob_drop_ls is None) or (len(prob_drop_ls)==len(n_h_ls)+1) 

    # Note 1:
    # true_lr = lr/(1+lr_decay*(epoch-n_early))

    # Note 2
    # L1/L2 = 1e-3 seems too high, L1/L2=0.0 is also not optimal
    # But any combination of L1,L2 from [0.0, 1e-4, 1e-5] is similar

    #################################################

    mlp_obj, data = main(dataset, n_epochs, n_epochs_early, n_h_ls, mbs,
                         lr, lr_decay, mom_early, mom_late, L1, L2, prob_drop_ls,
                         network_to_load, save_folder, keep_snapshots, save_all)