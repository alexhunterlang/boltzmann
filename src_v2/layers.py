"""
layers
"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano
import theano.tensor as T
from theano.tensor.nnet import sigmoid, softmax
import warnings
from utils import binomial_sample

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

msg = "numpy.ndarray size changed, may indicate binary incompatibility"
# can safely ignore this warning
# see https://groups.google.com/forum/#!topic/theano-users/A__NVIBYMxA
warnings.filterwarnings("ignore", message=msg)

#%%
class InputLayer(object):
    def __init__(self, data_input, n_in, mbs, np_rng, theano_rng, 
                 prob_drop = 0.0):
    
        self.data_input = data_input
        self.n_in = n_in
        self.n_out = self.n_in
        self.index = 0
        self.name = str(self.index)
        self.mbs = mbs
        self.np_rng = np_rng
        self.theano_rng = theano_rng
        self.layer_type = 'input'
        self.prob_drop = prob_drop
        self.IS_dropout = (self.prob_drop > 0.0)
        if self.IS_dropout:
            self.dropout = DropoutFilter(self)
        else:
            self.dropout = None
        
    #%%
    def get_output(self,  data_input = None, IS_dropout = True):
        if data_input is None:
            data_input = self.data_input
            
        if IS_dropout and self.IS_dropout:
            data_input = self.dropout.get_output(data_input,IS_dropout)
        
        return data_input   
        
    #%%
    def update_dropout(self, updates):
        
        if self.IS_dropout:
            updates = self.dropout.update_dropout(updates)
        
        return updates

#%%
#%%
class DropoutFilter(object):
    def __init__(self,layer_input):
        
        self.layer_input = layer_input
        self.n_in = layer_input.n_out
        self.n_out = self.n_in
        self.index = layer_input.index
        self.name = str(self.index) 
        self.mbs = layer_input.mbs
        self.np_rng = layer_input.np_rng
        self.theano_rng = layer_input.theano_rng
        self.prob_drop = layer_input.prob_drop
        
        value = ((1-self.prob_drop)*np.ones((self.mbs,self.n_in))).astype(FLOAT)  
        name = 'p_keep' + self.name                             
        self.p_keep = theano.shared(value = value, name = name, borrow = True)
        
        init_sample_keep = binomial_sample(self.theano_rng,self.p_keep)
        value = init_sample_keep.eval().astype(FLOAT)
        name = 'mask' + self.name
        self.mask = theano.shared(value = value, name = name, borrow=True)
        
    #%%
    def get_output(self, data_input, IS_dropout = True):
      
        if IS_dropout:
            output = data_input*self.mask
        else:
            output = data_input
            
        return output
        
    #%%
    def update_dropout(self, updates):
        
        updates[self.mask] = binomial_sample(self.theano_rng, self.p_keep)

        return updates             
        
#%%
#%%
class HiddenLayer(object):
    def __init__(self, layer_input, n_out, index = None, L1 = 0.0, L2 = 0.0,
                 prob_drop = 0.0, nonlin = sigmoid, W = None, b = None):  
                 
        self.layer_input = layer_input
        self.n_in = layer_input.n_out                 
        self.n_out = n_out
        self.layer_type = 'hidden'

        self.index_input = self.layer_input.index                 
        if index is None:
            self.index = self.index_input+1
        else:
            assert index > 0
            # input data is layer 0 to have same notation as DBN/DBM                 
            self.index = index
        self.name = str(index)
        self.name_W = str(self.index_input)+str(index)
        
        self.nonlin = nonlin                 
        self.np_rng = layer_input.np_rng
        self.theano_rng = layer_input.theano_rng
        self.mbs = layer_input.mbs
        self.prob_drop = prob_drop
        self.prob_drop_input = self.layer_input.prob_drop
        self.IS_dropout = (self.prob_drop>0.0)
        if self.IS_dropout:
            self.dropout = DropoutFilter(self)
        else:
            self.dropout = None
        
        self.L1 = L1
        self.L2 = L2
        self.IS_L1 = (L1 > 0.0)
        self.IS_L2 = (L2 > 0.0)
        self.L1_T = T.cast(L1, dtype = FLOAT)
        self.L2_T = T.cast(L2, dtype = FLOAT)
        
        name = 'W'+self.name_W
        if W is None:
            # create shared variable for weights
            # Random initialization to full rank matrix
            # Approximate implementation of ref [3]
            gain = 1
            U = np.random.normal(size=(self.n_in,self.n_in))/np.sqrt(self.n_in)
            V = np.random.normal(size=(self.n_out,self.n_out))/np.sqrt(self.n_out)    
            S = np.zeros((self.n_in,self.n_out))
            np.fill_diagonal(S,gain)
            init_W = np.array(U.dot(S).dot(V),dtype=FLOAT)
        else:
            msg1 = 'W'+self.name_W+' has mismatch between '
            if self.n_in != W.shape[0]:
                msg2 = 'n_in of layer_input and init_W.shape[0]'                
                raise ValueError('Mismatched Dim Error', msg1+msg2) 
            elif self.n_out != W.shape[1]:
                msg2 = 'n_out of init() and init_W.shape[1]'                
                raise ValueError('Mismatched Dim Error', msg1+msg2)                 
            else:
                init_W = W    
        self.W = theano.shared(value=init_W, name=name, borrow=True)
        self.W_final = self.W*(1-T.cast(self.prob_drop_input,dtype=FLOAT))        
        
        name = 'b'+self.name
        if b is None:
            init_b = np.zeros(self.n_out, dtype=FLOAT)
        else:
            if self.n_out != b.shape[0]:
                msg1 = 'b'+self.name+' has mismatch between '
                msg2 = 'n_out of init() and init_b.shape[0]'                
                raise ValueError('Mismatched Dim Error', msg1+msg2) 
            else:
                init_b = b
        self.b = theano.shared(value=init_b, name=name, borrow=True)         
        
        # Initial shared variables for momentum
        name = 'vel_W'+self.name_W
        value = np.zeros((self.n_in,self.n_out),dtype=FLOAT)
        self.vel_W =  theano.shared(value=value, name=name, borrow=True)
        name = 'vel_b'+self.name
        self.vel_b =  theano.shared(value=np.zeros(self.n_out,dtype=FLOAT),
                                     name=name, borrow=True)
        
        # Best parameters
        name = 'best_W'+self.name_W
        value = np.zeros((self.n_in,self.n_out),dtype=FLOAT)
        self.best_W =  theano.shared(value=value, name=name, borrow=True)
        name = 'best_b'+self.name
        self.best_b =  theano.shared(value=np.zeros(self.n_out,dtype=FLOAT),
                                     name=name, borrow=True)
        
        # parameters of the model
        self.params = [self.W, self.b]
        self.vparams = [self.vel_W, self.vel_b]
        self.best = [self.best_W, self.best_b]
        
    #%%
    def get_cost(self, want_np=False):
        
        if self.IS_L1 and not self.IS_L2:
            cost = self.L1_T*T.sum(T.abs_(self.W_final))     
        elif not self.IS_L1 and self.IS_L2:
            cost = 0.5*self.L2_T*T.sum(T.pow(self.W_final,2))
        elif self.IS_L1 and self.IS_L2:
            cost = self.L1_T*T.sum(T.abs_(self.W_final)) +\
                    0.5*self.L2_T*T.sum(T.pow(self.W_final,2))
        else:
            cost = T.cast(0.0,dtype=FLOAT)

        if want_np:
            cost = cost.eval()

        return cost        
        
    #%%
    def update_best(self):
        self.best_W.set_value(self.W_final.eval())
        self.best_b.set_value(self.b.get_value(borrow=False))
         
    #%%
    def get_output(self, data_input = None, IS_dropout = True):
        
        if data_input is None:
            d_in = self.layer_input.get_output(data_input,IS_dropout)
        else:
            d_in = data_input
            
        if IS_dropout:
            W = self.W
        else:
            W = self.W_final            
            
        z = T.dot(d_in, W) + self.b                 
        
        output = self.nonlin(z)     
        if IS_dropout and self.IS_dropout:
            output = self.dropout.get_output(output,IS_dropout)
        
        return output
        
    #%%
    def update_dropout(self, updates):
        
        if self.IS_dropout:
            updates = self.dropout.update_dropout(updates)
        
        return updates        
        
    #%%
    def output_W(self):
        return self.W_final.eval()          
        
    #%%
    def output_b(self):
        return self.b.get_value()  

#%%
#%%
class OutputLayer(object):
    def __init__(self, layer_input, n_out, index = None, L1 = 0.0, L2 = 0.0,
                 W = None, b = None):
    
        # For now this is only a softmax layer    
    
        self.layer_input = layer_input
        self.n_in = layer_input.n_out                 
        self.n_out = n_out
        self.layer_type = 'output'

        self.index_input = self.layer_input.index                 
        if index is None:
            self.index = self.index_input+1
        else:
            assert index>0
            # input data is layer 0 to have same notation as DBN/DBM                 
            self.index = index
        self.name = str(index)
        self.name_W = str(self.index_input)+str(index)
        
        self.nonlin = softmax                 
        self.np_rng = layer_input.np_rng
        self.theano_rng = layer_input.theano_rng
        self.mbs = layer_input.mbs
        self.prob_drop_input = self.layer_input.prob_drop
        
        self.L1 = L1
        self.L2 = L2
        self.IS_L1 = (L1 > 0.0)
        self.IS_L2 = (L2 > 0.0)
        self.L1_T = T.cast(L1, dtype = FLOAT)
        self.L2_T = T.cast(L2, dtype = FLOAT)
        
        name = 'W'+self.name_W
        if W is None:
            # create shared variable for weights
            # Random initialization to full rank matrix
            # Approximate implementation of ref [3]
            gain = 1
            U = np.random.normal(size=(self.n_in,self.n_in))/np.sqrt(self.n_in)
            V = np.random.normal(size=(self.n_out,self.n_out))/np.sqrt(self.n_out)    
            S = np.zeros((self.n_in,self.n_out))
            np.fill_diagonal(S,gain)
            init_W = np.array(U.dot(S).dot(V),dtype=FLOAT)
        else:
            msg1 = 'W'+self.name_W+' has mismatch between '
            if self.n_in != W.shape[0]:
                msg2 = 'n_in of layer_input and init_W.shape[0]'                
                raise ValueError('Mismatched Dim Error', msg1+msg2) 
            elif self.n_out != W.shape[1]:
                msg2 = 'n_out of init() and init_W.shape[1]'                
                raise ValueError('Mismatched Dim Error', msg1+msg2)                 
            else:
                init_W = W    
        self.W = theano.shared(value=init_W, name=name, borrow=True)
        self.W_final = self.W*(1-T.cast(self.prob_drop_input,dtype=FLOAT))        
        
        name = 'b'+self.name
        if b is None:
            init_b = np.zeros(self.n_out, dtype=FLOAT)
        else:
            if self.n_out != b.shape[0]:
                msg1 = 'b'+self.name+' has mismatch between '
                msg2 = 'n_out of init() and init_b.shape[0]'                
                raise ValueError('Mismatched Dim Error', msg1+msg2) 
            else:
                init_b = b
        self.b = theano.shared(value=init_b, name=name, borrow=True)         
        
        # Initial shared variables for momentum
        name = 'vel_W'+self.name_W
        value = np.zeros((self.n_in,self.n_out),dtype=FLOAT)
        self.vel_W =  theano.shared(value=value, name=name, borrow=True)
        name = 'vel_b'+self.name
        self.vel_b =  theano.shared(value=np.zeros(self.n_out,dtype=FLOAT),
                                     name=name, borrow=True)
        
        # Best parameters
        name = 'best_W'+self.name_W
        value = np.zeros((self.n_in,self.n_out),dtype=FLOAT)
        self.best_W =  theano.shared(value=value, name=name, borrow=True)
        name = 'best_b'+self.name
        self.best_b =  theano.shared(value=np.zeros(self.n_out,dtype=FLOAT),
                                     name=name, borrow=True)
        
        # parameters of the model
        self.params = [self.W, self.b]
        self.vparams = [self.vel_W, self.vel_b]
        self.best = [self.best_W, self.best_b]
        
    #%%
    def get_cost(self, want_np=False):
        
        if self.IS_L1 and not self.IS_L2:
            cost = self.L1_T*T.sum(T.abs_(self.W))     
        elif not self.IS_L1 and self.IS_L2:
            cost = 0.5*self.L2_T*T.sum(T.pow(self.W,2))
        elif self.IS_L1 and self.IS_L2:
            cost = self.L1_T*T.sum(T.abs_(self.W)) +\
                    0.5*self.L2_T*T.sum(T.pow(self.W,2))
        else:
            cost = T.cast(0.0,dtype=FLOAT)

        if want_np:
            cost = cost.eval()

        return cost        
        
    #%%
    def update_best(self):
        self.best_W.set_value(self.W_final.eval())
        self.best_b.set_value(self.b.get_value(borrow=False))

    #%%
    def get_output(self, data_input = None, IS_dropout = True):
        
        if data_input is None:
            d_in = self.layer_input.get_output(data_input,IS_dropout)
        else:
            d_in = data_input
            
        if IS_dropout:
            W = self.W
        else:
            W = self.W_final            
            
        z = T.dot(d_in, W) + self.b                 
        
        output = self.nonlin(z)     
        
        return output
        
    #%%
    def get_pred(self):
        
        output = self.get_output(data_input = None, IS_dropout = False)        
        
        pred_lbl = T.cast(T.argmax(output, axis=1),dtype=INT)

        return pred_lbl

    #%%
    def get_best_pred(self, data_input):

        z = T.dot(data_input, self.W_final) + self.b                 

        pred_lbl = T.cast(T.argmax(self.nonlin(z), axis=1), dtype=INT)      
        
        return pred_lbl

    #%%
    def output_W(self):
        return self.W_final.eval()          
        
    #%%
    def output_b(self):
        return self.b.get_value()  
        
    #%%
    def update_dropout(self,updates):
        return updates

    #%%
    def negative_log_likelihood(self, data_lbl, IS_dropout = True):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """

        p = self.get_output(data_input = None, IS_dropout = IS_dropout)

        return -T.mean(T.log(p)[T.arange(data_lbl.shape[0]), data_lbl])

    #%%
    def errors(self, data_lbl):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        pred_lbl = self.get_pred()
        
        assert data_lbl.dtype == 'int32'
        assert pred_lbl.dtype == 'int32'
        
        # check if y has same dimension of y_pred
        if data_lbl.ndim != pred_lbl.ndim:
            raise TypeError(
                'data_lbl should have the same shape as pred_lbl',
                ('data_lbl', data_lbl.type, 'pred_lbl', pred_lbl.type)
            )
        # check if y is of the correct datatype
        if data_lbl.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.neq(pred_lbl, data_lbl))
        else:
            raise NotImplementedError()  

    