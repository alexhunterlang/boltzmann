#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import theano
import theano.tensor as T

FLOAT = theano.config.floatX # should be float32 for GPU

#%%
class L1L2Regularizer(object):
    """Regularizer for L1 and L2 regularization.
    # Arguments
        l1: Float; L1 regularization factor.
        l2: Float; L2 regularization factor.
    """

    def __init__(self, l1=0., l2=0.):
        self.l1 = T.cast(l1, dtype=FLOAT)
        self.l2 = T.cast(l2, dtype=FLOAT)

    def __call__(self, x):
        regularization = 0
        if self.l1:
            regularization += T.sum(self.l1 * T.abs(x))
        if self.l2:
            regularization += T.sum(self.l2 * T.square(x))
        return regularization

    def get_config(self):
        return {'name': self.__class__.__name__,
                'l1': float(self.l1),
                'l2': float(self.l2)}

#%%
def l1(l=0.01):
    return L1L2Regularizer(l1=l)

#%%
def l2(l=0.01):
    return L1L2Regularizer(l2=l)

#%%
def l1l2(l1=0.01, l2=0.01):
    return L1L2Regularizer(l1=l1, l2=l2)

#%%
def get(identifier, kwargs=None):
    identifier = identifier.lower()
    if identifier=='l1':
        return l1(kwargs)
    elif identifier=='l2':
        return l2(kwargs)
    elif identifier=='l1l2':
        return l1l2(kwargs)
    else:
        raise NotImplementedError
