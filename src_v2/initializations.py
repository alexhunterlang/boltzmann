#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

#%%
def orthogonal(shape, scale=1.1):
    """Orthogonal initializer.
    # References
        Saxe et al., http://arxiv.org/abs/1312.6120
    """
    flat_shape = (shape[0], np.prod(shape[1:]))
    a = np.random.normal(0.0, 1.0, flat_shape)
    u, _, v = np.linalg.svd(a, full_matrices=False)
    # Pick the one with the correct shape.
    q = u if u.shape == flat_shape else v
    q = q.reshape(shape)
    
    value = (scale * q[:shape[0], :shape[1]]).astype(FLOAT)

    return value

def zero(shape):
    value = np.zeros(shape, dtype=FLOAT)
    return value
