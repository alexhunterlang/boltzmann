""" Code that summarizes parameter searches """

#%% Imports
import os
import numpy as np
from shutil import copyfile

#%% Parameters

# TODO: when I change the precision of the accuracy, need to keep more digits in this code

# example text lr#_d#_L1_#_L2_#
l1_ls = ['0','3','4','5']
l2_ls = ['0','3','4','5']
l1_array = np.array([0,3,4,5])
l2_array = np.array([0,3,4,5])
lr_ls = ['01']
d_ls = ['1']

results_folder = '../results/rbm_nest_mom_lr01'
summary_folder = 'summary'

#%% code
out_folder = results_folder+'/'+summary_folder 
os.mkdir(out_folder) # general folder
os.mkdir(out_folder+'/samples') # keep final sample
os.mkdir(out_folder+'/models') # keep final model
os.mkdir(out_folder+'/samples_best') # keep final sample
os.mkdir(out_folder+'/models_best') # keep final model

for lr in lr_ls:
    for d in d_ls:
        cur_out = 'lr'+lr+'_d'+d
        # this summarizes the final valid accuracy 
        table = np.zeros((len(l1_ls)+1,len(l2_ls)+1))
        table[1:,0] = l1_array
        table[0,1:] = l2_array
        # this summarizes the valid accuracy at the best log_p_valid        
        table_best = np.zeros((len(l1_ls)+1,len(l2_ls)+1))
        table_best[1:,0] = l1_array
        table_best[0,1:] = l2_array
        
        table_epoch = np.zeros((len(l1_ls)+1,len(l2_ls)+1))
        table_epoch[1:,0] = l1_array
        table_epoch[0,1:] = l2_array
        
        table_pv = np.zeros((len(l1_ls)+1,len(l2_ls)+1))
        table_pv[1:,0] = l1_array
        table_pv[0,1:] = l2_array
        
        # copies samples and models to here        
        sample_out = out_folder+'/samples/'+ cur_out
        os.mkdir(sample_out)
        model_out = out_folder+'/models/'+ cur_out
        os.mkdir(model_out)
        sample_best_out = out_folder+'/samples_best/'+ cur_out
        os.mkdir(sample_best_out)
        model_best_out = out_folder+'/models_best/'+ cur_out
        os.mkdir(model_best_out)
        for i,l1 in enumerate(l1_ls):
            for j,l2 in enumerate(l2_ls):
                fname = 'lr'+lr+'_d'+d+'_L1_'+l1+'_L2_'+l2
                try:
                    with open(results_folder+'/'+fname+'/accuracy/valid_report.txt','r') as f:
                        read_data = f.readlines()
                    
                    # this is the last valid accuracy
                    table[i+1,j+1] = float(read_data[-3][16:22])
            
                    # Find the best log_p_valid
                    with open(results_folder+'/'+fname+'/ais/log_p_valid_detailed.txt','r') as f:
                        temp = np.loadtxt(f,  dtype='str',delimiter='\t')
                    temp = temp[:,0:4]
                    data_ais = np.zeros(temp.shape)
                    for ii in range(data_ais.shape[0]):
                        for jj in range(data_ais.shape[1]):
                            data_ais[ii,jj] = float(temp[ii,jj])                       
                    index = np.argmax(data_ais[:,3])
                    # record best log_p_valid 
                    table_pv[i+1,j+1] = data_ais[index,3]
                    # record accuracy
                    best_epoch = int(data_ais[index,0])
                    table_epoch[i+1,j+1] = best_epoch
                    wanted_epoch =  read_data.index("Epoch {}\n".format(best_epoch))
                    table_best[i+1,j+1] = float(read_data[wanted_epoch+14][16:22])
            
                    # copy over final sample
                    sample_in = results_folder+'/'+fname+'/samples'
                    all_samples = [ss for ss in os.listdir(sample_in) if ss.startswith('epoch')]
                    epoch = [int(ss.split('_')[0][5:]) for ss in all_samples]   
                    last = max(epoch)
                    src = sample_in+'/epoch'+str(last)+'_samples.pdf'
                    copyfile(src,sample_out+'/'+fname+'.pdf')
            
                    # copy over final model
                    model_in = results_folder+'/'+fname+'/saved_model'
                    all_models = [m for m in os.listdir(model_in) if m.startswith('model')]
                    epoch = [int(ss.split('.')[0][6:]) for ss in all_models]   
                    last = max(epoch)
                    src = model_in+'/model_'+str(last)+'.npz'
                    copyfile(src,model_out+'/'+fname+'.npz')
                    
                    # copy over best sample
                    sample_in = results_folder+'/'+fname+'/samples'
                    src = sample_in+'/epoch'+str(best_epoch)+'_samples.pdf'
                    copyfile(src,sample_best_out+'/'+fname+'.pdf')
            
                    # copy over final model
                    model_in = results_folder+'/'+fname+'/saved_model'
                    src = model_in+'/model_'+str(best_epoch)+'.npz'
                    copyfile(src,model_best_out+'/'+fname+'.npz')
                    
                except:
                    pass
        
        # saves the summary table
        fname = 'lr'+lr+'_d'+d+'_summary.txt'
        np.savetxt(out_folder+'/'+fname, table, fmt='%.4f',delimiter='\t')
        fname = 'lr'+lr+'_d'+d+'_acc_at_best_p_valid.txt'
        np.savetxt(out_folder+'/'+fname, table_best, fmt='%.4f',delimiter='\t')
        fname = 'lr'+lr+'_d'+d+'_epoch_of_best_p_valid.txt'
        np.savetxt(out_folder+'/'+fname, table_epoch, fmt='%.2f',delimiter='\t')
        fname = 'lr'+lr+'_d'+d+'_best_p_valid.txt'
        np.savetxt(out_folder+'/'+fname, table_pv, fmt='%.2f',delimiter='\t')