#%%
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import warnings
from six.moves import zip

from keras import backend as K
from keras import initializations, regularizers, constraints

#%%
def to_list(x):
    """This normalizes a list/tensor into a list.
    If a tensor is passed, we return
    a list of size 1 containing the tensor.
    """
    if isinstance(x, list):
        return x
    return [x]

#%%
class NeuralUnit(object):
    """Abstract base neural unit object.
    # Properties
        name: String
        trainable: Boolean, whether the layer weights
            will be updated during training.
        uses_learning_phase: Whether any operation
            of the layer uses `K.in_training_phase()`
            or `K.in_test_phase()`.
        dimension: shape of unit
        trainable_weights: List of variables.
        non_trainable_weights: List of variables.
        weights: The concatenation of the lists trainable_weights and
            non_trainable_weights (in this order).
        losses: Dict mapping weights to losses.
        constraints: Dict mapping weights to constraints.
    # Methods
        get_weights()
        set_weights(weights)
    
    """

    def __init__(self, **kwargs):
        # These properties should have been set
        # by the child class, as appropriate.
        if not hasattr(self, 'uses_learning_phase'):
            self.uses_learning_phase = False

        # These properties will be set upon call of self.build()
        if not hasattr(self, '_trainable_weights'):
            self._trainable_weights = []
        if not hasattr(self, '_non_trainable_weights'):
            self._non_trainable_weights = []
        if not hasattr(self, 'losses'):
            self.losses = []
        if not hasattr(self, 'constraints'):
            self.constraints = {}  # dict {tensor: constraint instance}

        name = kwargs.get('name')
        if not name:
            prefix = self.__class__.__name__.lower()
            name = prefix + '_' + str(K.get_uid(prefix))
        self.name = name

        self.trainable = kwargs.get('trainable', True)

    @property
    def trainable_weights(self):
        trainable = getattr(self, 'trainable', True)
        if trainable:
            return self._trainable_weights
        else:
            return []

    @trainable_weights.setter
    def trainable_weights(self, weights):
        self._trainable_weights = weights

    @property
    def non_trainable_weights(self):
        trainable = getattr(self, 'trainable', True)
        if not trainable:
            return self._trainable_weights + self._non_trainable_weights
        else:
            return self._non_trainable_weights

    @non_trainable_weights.setter
    def non_trainable_weights(self, weights):
        self._non_trainable_weights = weights

    def add_weight(self, shape, initializer, name=None,
                   trainable=True,
                   regularizer=None,
                   constraint=None):
        """Adds a weight variable to the layer.
        # Arguments
            shape: The shape tuple of the weight.
            initializer: An Initializer instance (callable).
            trainable: A boolean, whether the weight should
                be trained via backprop or not (assuming
                that the layer itself is also trainable).
            regularizer: An optional Regularizer instance.
        """
        initializer = initializations.get(initializer)
        weight = initializer(shape, name=name)
        if regularizer is not None:
            self.add_loss(regularizer(weight))
        if constraint is not None:
            self.constraints[weight] = constraint
        if trainable:
            self._trainable_weights.append(weight)
        else:
            self._non_trainable_weights.append(weight)
        return weight
 
    def add_loss(self, losses, inputs=None):
        if losses is None:
            return
        # Update self.losses
        losses = to_list(losses)
        if not hasattr(self, 'losses'):
            self.losses = []
        self.losses += losses

    def add_update(self, updates, inputs=None):
        if updates is None:
            return
        # Update self.updates
        updates = to_list(updates)
        if not hasattr(self, 'updates'):
            self.updates = []
        self.updates += updates
        # TODO: figure out what to do with inputs

    @property
    def weights(self):
        return self.trainable_weights + self.non_trainable_weights

    def set_weights(self, weights):
        """Sets the weights of the layer, from Numpy arrays.
        # Arguments
            weights: a list of Numpy arrays. The number
                of arrays and their shape must match
                number of the dimensions of the weights
                of the layer (i.e. it should match the
                output of `get_weights`).
        """
        params = self.weights
        if len(params) != len(weights):
            raise ValueError('You called `set_weights(weights)` on layer "' +
                             self.name +
                             '" with a  weight list of length ' +
                             str(len(weights)) +
                             ', but the layer was expecting ' +
                             str(len(params)) +
                             ' weights. Provided weights: ' +
                             str(weights)[:50] + '...')
        if not params:
            return
        weight_value_tuples = []
        param_values = K.batch_get_value(params)
        for pv, p, w in zip(param_values, params, weights):
            if pv.shape != w.shape:
                raise ValueError('Layer weight shape ' +
                                 str(pv.shape) +
                                 ' not compatible with '
                                 'provided weight shape ' + str(w.shape))
            weight_value_tuples.append((p, w))
        K.batch_set_value(weight_value_tuples)

    def get_weights(self):
        """Returns the current weights of the layer,
        as a list of numpy arrays.
        """
        params = self.weights
        return K.batch_get_value(params)

#%%
class Layer(NeuralUnit):
    def __init__(self, dim, init='orthogonal', weights=None,
                 regularizer=None, constraint=None, persist = False,
                 dropout_p = 0.0, mbs = None, **kwargs):
        
        super(Layer,self).__init__(**kwargs)
        
        if isinstance(dim, tuple):
            self.dim = dim[0]
            self.shape = dim
        else:
            self.dim = dim
            self.shape = (dim,)
        self.init = initializations.get(init)
        self.regularizer = regularizers.get(regularizer)
        self.constraint = constraints.get(constraint)
        self.persist = persist
        self.dropout_p = dropout_p
        self.mbs = mbs
        
        # this gets the bias set up
        self.b = self.add_weight(self.shape, self.init,
                                 name='{}_b'.format(self.name),
                                trainable=self.trainable,
                                regularizer=self.regularizer,
                                constraint=self.constraint)
        if weights is not None:
            self.set_weights(weights)
        
        # need to handle dropout
        assert 0<= self.dropout_p < 1
        if 0. < self.dropout_p < 1.:
            self.uses_learning_phase = True
            self.dropout_mask = self.add_weight(self.shape,
                                                initializations.get('zero'),
                                                name='{}_dropout'.format(self.name),
                                                trainable=False,
                                                regularizer=None,
                                                constraint=None)
            K.set_value(self.dropout_mask,
                        K.random_binomial(self.shape, p=self.dropout_p, dtype=K.floatx()))
        # TODO: make an update dropout method
        
        
        # TODO: add in persist load
        if self.persist:
            assert self.mbs is not None
            # TODO: does it matter about learning phase?
            self.persist_state = self.add_weight((self.mbs, self.dim),
                                                initializations.get('zero'),
                                                name='{}_persist'.format(self.name),
                                                trainable=False,
                                                regularizer=None,
                                                constraint=None)
            # TODO: this gets initialized later
        
        # TODO: add all synapses
        

#%%
class Synapse(NeuralUnit):
    def __init__(self, shape, init='orthogonal', weights=None,
                 regularizer=None, constraint=None, **kwargs):
        
        super(Synapse,self).__init__(**kwargs)
        self.shape = shape
        self.input_dim = shape[0]
        self.output_dim = shape[1]
        
        self.init = initializations.get(init)
        self.regularizer = regularizers.get(regularizer)
        self.constraint = constraints.get(constraint)
        
        self.W = self.add_weight(self.shape, self.init,
                                 name='{}_W'.format(self.name),
                                trainable=self.trainable,
                                regularizer=self.regularizer,
                                constraint=self.constraint)
        
        if weights is not None:
            self.set_weights(weights)
        
        