#%%
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import warnings
from six.moves import zip

from keras import backend as K
from keras import initializations

#%%
class Layer(object):
    """Abstract base layer class.
    # Properties
        name: String, must be unique within a model.
        input_spec: List of InputSpec class instances
            each entry describes one required input:
                - ndim
                - dtype
            A layer with `n` input tensors must have
            an `input_spec` of length `n`.
        trainable: Boolean, whether the layer weights
            will be updated during training.
        uses_learning_phase: Whether any operation
            of the layer uses `K.in_training_phase()`
            or `K.in_test_phase()`.
        input_shape: Shape tuple. Provided for convenience,
            but note that there may be cases in which this
            attribute is ill-defined (e.g. a shared layer
            with multiple input shapes), in which case
            requesting `input_shape` will raise an Exception.
            Prefer using `layer.get_input_shape_for(input_shape)`,
            or `layer.get_input_shape_at(node_index)`.
        output_shape: Shape tuple. See above.
        inbound_nodes: List of nodes.
        outbound_nodes: List of nodes.
        supports_masking: Boolean.
        input, output: Input/output tensor(s). Note that if the layer is used
            more than once (shared layer), this is ill-defined
            and will raise an exception. In such cases, use
            `layer.get_input_at(node_index)`.
        input_mask, output_mask: Same as above, for masks.
        trainable_weights: List of variables.
        non_trainable_weights: List of variables.
        weights: The concatenation of the lists trainable_weights and
            non_trainable_weights (in this order).
        constraints: Dict mapping weights to constraints.
    # Methods
        call(x, mask=None): Where the layer's logic lives.
        __call__(x, mask=None): Wrapper around the layer logic (`call`).
            If x is a Keras tensor:
                - Connect current layer with last layer from tensor:
                    `self.add_inbound_node(last_layer)`
                - Add layer to tensor history
            If layer is not built:
                - Build from x._keras_shape
        get_weights()
        set_weights(weights)
        get_config()
        count_params()
        get_output_shape_for(input_shape)
        compute_mask(x, mask)
        get_input_at(node_index)
        get_output_at(node_index)
        get_input_shape_at(node_index)
        get_output_shape_at(node_index)
        get_input_mask_at(node_index)
        get_output_mask_at(node_index)
    # Class Methods
        from_config(config)
    # Internal methods:
        build(input_shape)
        add_inbound_node(layer, index=0)
        create_input_layer()
        assert_input_compatibility()
    """

    def __init__(self, **kwargs):
        # These properties should have been set
        # by the child class, as appropriate.
        if not hasattr(self, 'uses_learning_phase'):
            self.uses_learning_phase = False

        # These lists will be filled via successive calls
        # to self.add_synapse().
        self.inbound_synapse = []
        self.outbound_synapse = []

        # These properties will be set upon call of self.build()
        if not hasattr(self, '_trainable_weights'):
            self._trainable_weights = []
        if not hasattr(self, '_non_trainable_weights'):
            self._non_trainable_weights = []
        if not hasattr(self, 'losses'):
            self.losses = []
        if not hasattr(self, 'constraints'):
            self.constraints = {}  # dict {tensor: constraint instance}

        name = kwargs.get('name')
        if not name:
            prefix = self.__class__.__name__.lower()
            name = prefix + '_' + str(K.get_uid(prefix))
        self.name = name

        self.trainable = kwargs.get('trainable', True)

    @property
    def trainable_weights(self):
        trainable = getattr(self, 'trainable', True)
        if trainable:
            return self._trainable_weights
        else:
            return []

    @trainable_weights.setter
    def trainable_weights(self, weights):
        self._trainable_weights = weights

    @property
    def non_trainable_weights(self):
        trainable = getattr(self, 'trainable', True)
        if not trainable:
            return self._trainable_weights + self._non_trainable_weights
        else:
            return self._non_trainable_weights

    @non_trainable_weights.setter
    def non_trainable_weights(self, weights):
        self._non_trainable_weights = weights

    @property
    def regularizers(self):
        warnings.warn('The `regularizers` property of '
                      'layers/models is deprecated. '
                      'Regularization losses are now managed via the `losses` '
                      'layer/model property.')
        return []

    @regularizers.setter
    def regularizers(self, _):
        warnings.warn('The `regularizers` property of layers/models '
                      'is deprecated. '
                      'Regularization losses are now managed via the `losses` '
                      'layer/model property.')

    def add_weight(self, shape, initializer, name=None,
                   trainable=True,
                   regularizer=None,
                   constraint=None):
        """Adds a weight variable to the layer.
        # Arguments
            shape: The shape tuple of the weight.
            initializer: An Initializer instance (callable).
            trainable: A boolean, whether the weight should
                be trained via backprop or not (assuming
                that the layer itself is also trainable).
            regularizer: An optional Regularizer instance.
        """
        initializer = initializations.get(initializer)
        weight = initializer(shape, name=name)
        if regularizer is not None:
            self.add_loss(regularizer(weight))
        if constraint is not None:
            self.constraints[weight] = constraint
        if trainable:
            self._trainable_weights.append(weight)
        else:
            self._non_trainable_weights.append(weight)
        return weight

    def call(self, x):
        """This is where the layer's logic lives.
        # Arguments
            x: input tensor, or list/tuple of input tensors.
        # Returns:
            A tensor or list/tuple of tensors.
        """
        return x

    def __call__(self, x):
        return self.call(x)

    def add_synapse(self):
        pass
 
    def add_loss(self, losses, inputs=None):
        if losses is None:
            return
        # Update self.losses
        losses = to_list(losses)
        if not hasattr(self, 'losses'):
            self.losses = []
        try:
            self.losses += losses
        except AttributeError:
            # In case self.losses isn't settable
            # (i.e. it's a getter method).
            # In that case the `losses` property is
            # auto-computed and shouldn't be set.
            pass
        # Update self._per_input_updates
        if not hasattr(self, '_per_input_losses'):
            self._per_input_losses = {}
        if inputs is not None:
            inputs_hash = object_list_uid(inputs)
        else:
            # Updates indexed by None are unconditional
            # rather than input-dependent
            inputs_hash = None
        if inputs_hash not in self._per_input_losses:
            self._per_input_losses[inputs_hash] = []
        self._per_input_losses[inputs_hash] += losses

    def add_update(self, updates, inputs=None):
        if updates is None:
            return
        # Update self.updates
        updates = to_list(updates)
        if not hasattr(self, 'updates'):
            self.updates = []
        try:
            self.updates += updates
        except AttributeError:
            # In case self.updates isn't settable
            # (i.e. it's a getter method).
            # In that case the `updates` property is
            # auto-computed and shouldn't be set.
            pass
        # Update self._per_input_updates
        if not hasattr(self, '_per_input_updates'):
            self._per_input_updates = {}
        if inputs is not None:
            inputs_hash = object_list_uid(inputs)
        else:
            # Updates indexed by None are unconditional
            # rather than input-dependent
            inputs_hash = None
        if inputs_hash not in self._per_input_updates:
            self._per_input_updates[inputs_hash] = []
        self._per_input_updates[inputs_hash] += updates

    def get_updates_for(self, inputs):
        if not hasattr(self, '_per_input_updates'):
            return []
        if inputs is not None:
            inputs_hash = object_list_uid(inputs)
        else:
            inputs_hash = None
        if inputs_hash in self._per_input_updates:
            return self._per_input_updates[inputs_hash]
        return []

    def get_losses_for(self, inputs):
        if not hasattr(self, '_per_input_losses'):
            return []
        if inputs is not None:
            inputs_hash = object_list_uid(inputs)
        else:
            inputs_hash = None
        if inputs_hash in self._per_input_losses:
            return self._per_input_losses[inputs_hash]
        return []

    @property
    def weights(self):
        return self.trainable_weights + self.non_trainable_weights

    def set_weights(self, weights):
        """Sets the weights of the layer, from Numpy arrays.
        # Arguments
            weights: a list of Numpy arrays. The number
                of arrays and their shape must match
                number of the dimensions of the weights
                of the layer (i.e. it should match the
                output of `get_weights`).
        """
        params = self.weights
        if len(params) != len(weights):
            raise ValueError('You called `set_weights(weights)` on layer "' +
                             self.name +
                             '" with a  weight list of length ' +
                             str(len(weights)) +
                             ', but the layer was expecting ' +
                             str(len(params)) +
                             ' weights. Provided weights: ' +
                             str(weights)[:50] + '...')
        if not params:
            return
        weight_value_tuples = []
        param_values = K.batch_get_value(params)
        for pv, p, w in zip(param_values, params, weights):
            if pv.shape != w.shape:
                raise ValueError('Layer weight shape ' +
                                 str(pv.shape) +
                                 ' not compatible with '
                                 'provided weight shape ' + str(w.shape))
            weight_value_tuples.append((p, w))
        K.batch_set_value(weight_value_tuples)

    def get_weights(self):
        """Returns the current weights of the layer,
        as a list of numpy arrays.
        """
        params = self.weights
        return K.batch_get_value(params)
