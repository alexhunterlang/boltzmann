#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano
import os

import keras.backend as K
from keras.activations import sigmoid

from network import Network
from neural_components import Layer, Synapse

#%%
class DBM(Network):
    """ Deep boltzmann machine """
    def __init__(self, data_input, n_h_ls, topology, mbs = 128,
                 regularizer = None, constraint = None,
                 persist = False, dropout_p = 0.0):       
        
        self.train_class = 'unsuper'        
        
        self.data_input = data_input
        self.mbs = mbs
        self.regularizer = regularizer
        self.constraint = constraint
        self.persist = persist
        self.dropout_p = dropout_p
        
        self.activation = sigmoid
        
        self.n_h_ls = n_h_ls
        self.topology = topology
        
        # Figures out size of each layer
        self.n_v = data_input.size
        self.n_all = [self.n_v] + n_h_ls
        self.n_layers = len(self.n_all)
        self.n_output = n_h_ls[-1]
        self.n_h_ls = n_h_ls[:-1]
        
        # Create synapses     
        self.synapses_ls = []
        for i in range(len(self.n_all)-1):
            if network_to_load is not None:
                init_W = dbn_dict['W'+str(i)+str(i+1)]
            else:
                init_W = None
                
            synapse = Synapse(n_in = self.n_all[i], n_out = self.n_all[i+1],
                              index = i, init_W = init_W,
                              prob_drop = prob_drop, L1 = L1, L2 = L2)
            self.synapses_ls.append(synapse)
        
        # get lists of sets of parameters
        self.weight_ls = []
        self.weight_final_ls = []
        self.bias_ls = []
        self.vel_ls = []
        self.best_ls = []
        self.persist_ls = []
        for synapse in self.synapses_ls:
            self.weight_ls.append(synapse.W)
            self.weight_final_ls.append(synapse.W_final)
            self.vel_ls.append(synapse.vel_W)
            self.best_ls.append(synapse.best_W)
                
        # create neuron layers
        self.layers_ls = []
        temp_weight_ls = [None] + self.weight_ls + [None]
        for i in range(len(self.n_all)):
            if i==0:
                layer_type = 'input'
                layer_data = data
            elif i==len(self.n_all)-1:
                layer_type = 'output'
                layer_data = None
            else:
                layer_type = 'middle'
                layer_data = None
    
            if network_to_load is not None:
                init_b = dbn_dict['b'+str(i)]
            else:
                init_b = None                
                
            layer = Layer(self.n_all[i], i, layer_type, self.np_rng, self.theano_rng,
                          W_in = temp_weight_ls[i], W_out = temp_weight_ls[i+1],
                          mbs = self.mbs, prob_drop = self.prob_drop,
                          IS_persist = self.IS_persist, nonlin = self.nonlin,
                          init_b = init_b, init_persist = None, data = layer_data,
                          IS_dbm_adj = self.IS_dbm_adj_ls[i])
            self.layers_ls.append(layer)

        self.parts_ls = self.synapses_ls+self.layers_ls
               
        # Prepares for persistence
        if self.IS_persist:
            index = np.random.randint(low=0,high=self.n_samples,
                                      size=(self.mbs,))
            temp_data = data.train.data[index]   
            prob_ls = self.propup(temp_data, IS_dropout = False)
            prob_ls = [temp_data]+prob_ls
            for i in range(len(self.layers_ls)):
                layer = self.layers_ls[i]
                layer.persist.set_value(prob_ls[i].eval())

        # get lists of sets of parameters
        for layer in self.layers_ls:
            self.bias_ls.append(layer.b)
            self.vel_ls.append(layer.vel_b)
            self.best_ls.append(layer.best_b)
            if self.IS_persist:
                self.persist_ls.append(layer.persist) 
        self.params = self.weight_ls+self.bias_ls

    #%%
    def free_energy(self, vis):
        ''' Function to compute the free energy of a visible sample '''
        
        prob_ls = self.propup(vis, IS_dropout = False)
        prob_ls = [vis] + prob_ls

        return self.free_energy_given_h(prob_ls, IS_dropout = False)        

    #%%
    def free_energy_given_h(self, prob_ls, IS_dropout = False):
        """ Function for free energy given visible sample and 
        activations of hidden layers        
        """

        z_ls = []
        prob_ls = prob_ls+[None]
        for i in range(len(self.layers_ls[1:])):
            layer = self.layers_ls[i+1]
            z = layer.get_input(input_up = prob_ls[i], 
                                     input_down = prob_ls[i+2],
                                     IS_mf=True, IS_dropout=IS_dropout)
            z_ls.append(z)
    
        z = K.concatenate(z_ls,axis=1)
        vbias_term = K.dot(prob_ls[0], self.bias_ls[0])
        hidden_term = K.sum(K.log(1 + K.exp(z)), axis=1)
        
        return - hidden_term - vbias_term
    
    #%%
    def propup(self, vis, IS_dropout = False):
        """ Pass data up through network"""
        
        prob_ls = []  
        prob_up = vis            
        
        for layer in self.layers_ls[1:]:
            z = layer.get_input(input_up = prob_up, direction = 'up',
                                     IS_mf = True, IS_dropout = IS_dropout)
            prob_up = layer.get_output(z, IS_dropout)
            prob_ls.append(prob_up)               
                
        return prob_ls        
 
    #%%
    def parity_update(self, z_ls, prob_ls, sample_ls, start, IS_dropout,
                      IS_prob_input = True):
        ''' Updates either even or odd layers of synapses'''
        
        prob_out = [None]*len(prob_ls)        
        
        if IS_prob_input:
            input_ls = [None] + prob_ls + [None]
        else:
            sample_in_ls = []
            for p in prob_ls:
                s = binomial_sample(self.theano_rng, p)
                sample_in_ls.append(s)
            input_ls = [None] + sample_in_ls + [None]
        
        for i in range(start,len(input_ls)-2,2):
            layer = self.layers_ls[i]
            z = layer.get_input(input_up = input_ls[i], 
                                     input_self = input_ls[i+1], 
                                     input_down = input_ls[i+2],
                                     IS_mf = self.IS_mf,
                                     IS_dropout = IS_dropout) 
            prob = layer.get_output(z, IS_dropout)                                     
            sample = binomial_sample(self.theano_rng, prob)
            
            z_ls[i] = z
            prob_out[i] = prob
            sample_ls[i] = sample
            
        # Pass out the probs that don't change
        for i,p in enumerate(prob_out):
            if p is None:
                prob_out[i] = prob_ls[i]
        
        return z_ls, prob_out, sample_ls
        
    #%%
    def gibbs_odd_even_odd(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the odd hidden states'''

        prob_ls = list(args)
        z_ls = [None]*len(prob_ls)
        sample_ls = [None]*len(prob_ls)    
 
        # Update even
        z_ls, prob_ls, sample_ls = self.parity_update(
                                            z_ls, prob_ls, sample_ls, 0, 
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = False)
        
        # update odd
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 1,
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = False)

        return z_ls+prob_ls+sample_ls             
        

    #%%
    def gibbs_even_odd_even(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states'''
               
        prob_ls = list(args)               
        z_ls = [None]*len(prob_ls)
        sample_ls = [None]*len(prob_ls)    
 
        # Update odd
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 1,
                                            IS_dropout = False,
                                            IS_prob_input = True)
        
        # update even
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 0,
                                            IS_dropout = False,
                                            IS_prob_input = True)

        return z_ls+prob_ls+sample_ls
                
    #%%
    def gibbs_even_odd_even_given_v(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states
            but with visible state fixed '''
                  
        if self.IS_dropout:
            vis = self.data_input*self.layers_ls[0].mask
        else:
            vis = self.data_input                  
                  
        prob_ls = [vis] + list(args)         
               
        z_ls = [None]*len(prob_ls)
        sample_ls = [None]*len(prob_ls)    
 
        # Update odd
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 1,
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = True)
        
        # update even (not vis)
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 2,
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = True)

        return z_ls[1:]+prob_ls[1:]+sample_ls[1:]


    #%%
    def training_update(self, lr = 0.01, mom = 0.9):
        """
        This functions implements one step of CD-k or PCD-k with 
        Nesterov momentum

        :param lr: learning rate used to train the RBM

        :param mom: momentum coefficient for Nesterov momentum

        Returns a proxy for the cost and the updates dictionary. The
        dictionary contains the update rules for weights and biases but
        also an update of the shared variable used to store the persistent
        chain and dropout masks, if used.
        """

        mom = TT.cast(mom, dtype = FLOAT)
        lr = TT.cast(lr, dtype = FLOAT)

        prob_data, updates = self.pos_stats()
        
        z_v_model, prob_model, updates = self.neg_stats(prob_data, updates)
         
        cost = self.batch_pseudo_cost(prob_data, prob_model)         
        
        constant_ls = prob_data+prob_model
        
        grads = TT.grad(cost, self.params, consider_constant = constant_ls)
        
        for g, param, vel in zip(grads, self.params, self.vel_ls):
            new_vel = mom*vel - lr*g
            updates[vel] = new_vel
            if self.IS_nest:
                # Nesterov - ref [4]
                updates[param] = param + mom*new_vel - lr*g 
            else:
                # standard
                updates[param] = param + new_vel 
         
        # get batch cost measures
        recon = self.batch_recon_cost(z_v_model)
        
        # update masks for next minibatch
        if self.IS_dropout:
            updates = self.update_dropout(updates)        
                    
        return recon, cost, updates

    #%%
    def pos_stats(self):
        # compute positive phase
    
        if self.IS_dropout:
            vis = self.data_input*self.layers_ls[0].mask
        else:
            vis = self.data_input         
        
        # use propup to get initial mf activations
        prob_ls = self.propup(vis, self.IS_dropout)
                
        output_ls = [None]*len(prob_ls) + prob_ls + [None]*len(prob_ls)       
                
        scan_out, updates = theano.scan(fn = self.gibbs_even_odd_even_given_v, 
                                outputs_info = output_ls, 
                                n_steps = self.n_ups, name = 'scan_pos')              
        
        prob_data = scan_out[len(prob_ls):2*len(prob_ls)]
        for i in range(len(prob_data)):
            temp = prob_data[i]
            prob_data[i] = temp[-1]
        
        # include visible in prob_data
        prob_data = [vis] + prob_data
        
        return prob_data, updates
        
    #%%
    def neg_stats(self,prob_data,updates):
               
        # decide how to initialize persistent chain:
        if self.IS_persist:
            # for PCD, we initialize from the old state of the chain
            chain_start = self.get_output_persist()
        else:
            # for CD, we use the newly generated hidden sample
            chain_start = prob_data
        
        # perform actual negative phase
        output_ls = [None]*len(chain_start)+chain_start+[None]*len(chain_start)

        scan_out, scan_updates = theano.scan(self.gibbs_odd_even_odd, 
                                        outputs_info = output_ls, 
                                        n_steps = self.k, name = 'scan_neg')

        # merge updates
        updates = merge_dicts(updates, scan_updates)

        # Ref [2] recommends to always use probs for update statistics
        prob_model = scan_out[len(chain_start):2*len(chain_start)]
        for i in range(len(prob_model)):
            temp = prob_model[i]
            prob_model[i] = temp[-1]

        temp = scan_out[0]
        z_v_model = temp[-1]
        
        if self.IS_persist:
            # Ref [2] recommends to always use probs for update statistics
            updates = self.update_persist(prob_model,updates)
        
        return z_v_model, prob_model, updates
        
    #%%
    def batch_recon_cost(self,z_v_model):
        
        cross_entropy = -TT.mean(TT.sum(
                self.data_input * TT.log(sigmoid(z_v_model)) +
                (1 - self.data_input) *
                    TT.log(1 - sigmoid(z_v_model)), axis=1))
        return cross_entropy
    
    #%%
    def batch_pseudo_cost(self, prob_data, prob_model):
        # Pseudo cost
            
        fe_data = TT.mean(self.free_energy_given_h(prob_data, self.IS_dropout)) 
        fe_model = TT.mean(self.free_energy_given_h(prob_model, self.IS_dropout)) 
        cost = fe_data - fe_model        
        
        if self.has_norm_cost:
            cost += self.get_norm_cost()*self.batch_norm
            
        return cost
 
    #%%
    def pseudo_likelihood_cost_examples(self, bit_i_idx, cost):
        """
        Stochastic approximation to the pseudo-likelihood
        
        Noisy, need to look at moving average of result.        
        """

        # calculate free energy for the given bit configuration
        fe_xi = self.free_energy(self.xi_examples)

        # flip bit x_i of matrix xi and preserve all other bits x_{\i}
        # Equivalent to xi[:,bit_i_idx] = 1-xi[:, bit_i_idx], but assigns
        # the result to xi_flip, instead of working in place on xi.
        xi_flip = TT.set_subtensor(self.xi_examples[:, bit_i_idx], 
                                   1 - self.xi_examples[:, bit_i_idx])

        # calculate free energy with bit flipped
        fe_xi_flip = self.free_energy(xi_flip)

        # equivalent to e^(-FE(x_i)) / (e^(-FE(x_i)) + e^(-FE(x_{\i})))
        #n_v_TT = TT.cast(self.n_v, dtype=FLOAT)
        cost = cost + TT.mean(TT.log(sigmoid(fe_xi_flip - fe_xi)))

        return cost
        
    #%%
    def pseudo_likelihood_cost(self, bit_i_idx, cost):
        """
        Stochastic approximation to the pseudo-likelihood
        
        Noisy, need to look at moving average of result.        
        """

        # calculate free energy for the given bit configuration
        fe_xi = self.free_energy(self.xi)

        # flip bit x_i of matrix xi and preserve all other bits x_{\i}
        # Equivalent to xi[:,bit_i_idx] = 1-xi[:, bit_i_idx], but assigns
        # the result to xi_flip, instead of working in place on xi.
        xi_flip = TT.set_subtensor(self.xi[:, bit_i_idx], 1 - self.xi[:, bit_i_idx])

        # calculate free energy with bit flipped
        fe_xi_flip = self.free_energy(xi_flip)

        # equivalent to e^(-FE(x_i)) / (e^(-FE(x_i)) + e^(-FE(x_{\i})))
        #n_v_TT = TT.cast(self.n_v, dtype=FLOAT)
        cost = cost + TT.mean(TT.log(sigmoid(fe_xi_flip - fe_xi)))

        return cost
        
    #%%
    def output_act(self,vis):
        """
        Given input visible data, returns output activations as numpy array
        """        
        prob_ls = self.propup(vis, IS_dropout = False)
        output_ls = []
        for p in prob_ls:
            output_ls.append(p.eval())
        
        return output_ls

    #%%
    def output_nnet(self):
        # Save arrays to a dictionary
        # This successfully preserves names for each array
                
        d_out = {}    
        for i,synapse in enumerate(self.synapses_ls):
            key = 'W'+str(i)+str(i+1)
            d_out[key] = synapse.output_W()
        for i,layer in enumerate(self.layers_ls):
            key = 'b'+str(i)
            d_out[key] = layer.output_b()
            if self.IS_persist:
                key = 'persist'+str(i)
                d_out[key] = layer.output_persist()
            
        return d_out

    #%%
    def save_nnet(self,save_folder,filename):
        """ save network to a file """

        d_out = self.output_nnet()        

        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
        outfile = save_folder + '/' + filename
        np.savez_compressed(outfile, dbm_dict = d_out)
