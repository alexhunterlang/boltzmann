"""
This contains all necessary code to get data into useful objects.

Data is the object for a given dataset. It contains DataSlice objects that
correspond to train, valid, and test. 

There are also a series of functions to actually load datasets.

Code by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3
"""

#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import os
import gzip
import sys
from random import shuffle
import theano
import theano.tensor as  TT
import utils
import dbm
import warnings

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

#%% parameters
class Data(object):
    def __init__(self, dataset, mbs = None, 
                 load_network_dict = None, hid_layer=None):
                
        self.dataset = dataset
        
        # Check for the various data conditions
        if self.dataset == 'neural_network':
            assert load_network_dict is not None
            datadict = load_data_neural_network(load_network_dict, hid_layer)        
        elif self.dataset == 'MNIST':
            datadict = load_data_mnist()
        elif self.dataset == 'MNIST_binary':
            datadict = load_data_mnist_binary()
        elif self.dataset == 'MNIST_binary_double':
            datadict = load_data_mnist_binary_double()         
        elif self.dataset == 'Overfit_Training':
            datadict = load_data_overfit_train()    
        elif self.dataset == 'Overfit_Training_Binary':
            datadict = load_data_overfit_train_binary()
        elif self.dataset == 'Easy':
            datadict = load_data_easy()
        elif self.dataset == 'Noise':
            datadict = load_data_noise()                            
        else: 
            return NotImplementedError()      

        # Dim of input images
        shape = datadict['train.data'].shape
        if len(shape)==2:
            # This data is already rasterized so need to produce 2d
            x,y = utils.find_display_dim(shape[1])
            self.x = x
            self.y = y
        else:
            self.x = shape[2]
            self.y = shape[3]

        # Make slice type 
        # record some useful stats, assuming that valid and test are the same  
        name = 'train'
        self.train = DataSlice(datadict[name+'.data'], datadict[name+'.lbl'],
                               name, acc_log_reg_table(dataset,name))  
        name = 'valid'
        self.valid = DataSlice(datadict[name+'.data'],datadict[name+'.lbl'],
                               name, acc_log_reg_table(dataset,name)) 
        name = 'test'
        self.test = DataSlice(datadict[name+'.data'],datadict[name+'.lbl'],
                              name, acc_log_reg_table(dataset,name)) 
        self.n_ch = None
        self.n_input = self.train.data.shape.eval()[1]    
        self.n_cat = np.unique(self.train.lbl.eval()).size     
            
        # This sets the minibatch size
        if mbs is not None:
            self.mbs = mbs
            self.train.set_mbs(mbs)
            self.valid.set_mbs(mbs)
            self.test.set_mbs(mbs)

#%%
class DataSlice(object):
    """
    Subset of data, for example train, valid, or test data
    """ 
    
    def __init__(self, data, lbl, slicetype = 'unknown', acc_logreg = None):
        
        assert slicetype in ['train', 'valid', 'test', 'unknown']        
        
        dim0 = data.shape[0]
        dim_rest = np.prod(data.shape[1:])
        data = np.reshape(data,(dim0,dim_rest))        
                
        self.data = theano.shared(np.asarray(data,dtype=FLOAT),
                                     borrow=True)  
        # Theano needs floatX to push computations to gpu
        # But public labels should be integer as expected
        self.private_lbl = theano.shared(np.asarray(lbl,dtype=FLOAT),
                                         borrow=True)   
        
        self.lbl = TT.cast(self.private_lbl, INT)
        self.slicetype = slicetype
        self.acc_logreg = acc_logreg 
        self.n_samples = self.data.shape.eval()[0]
            
        # for iterable batches
        self.index_order = np.array(range(self.n_samples),dtype=INT)
        self.current_index = 0 
        self.mbs = 0    
        
        # store average value for each categories
        temp_data = self.data.get_value()
        temp_lbl = self.lbl.eval()            
        n_cat = np.unique(temp_lbl).size
        avg_data = np.zeros((n_cat,temp_data.shape[1]))
        for i in range(n_cat):
            index = np.where(i==temp_lbl)[0]
            avg_data[i,:] = np.mean(temp_data[index,:], axis=0)
        self.avg_data = avg_data.astype(FLOAT)
        
        
    def set_mbs(self,mbs):   
        self.mbs = mbs
        self.n_batches = np.int32(self.n_samples/self.mbs)
        # Keep track of actual sizes of batches
        self.size_batches = self.mbs*np.ones((self.n_batches,))
        leftover = np.remainder(self.n_samples,mbs)        
        self.n_samples_iter = self.n_samples
        if leftover>0:
            self.size_batches[-1] = leftover
            self.n_samples_iter -= leftover
            warn1 = 'Minibatch size does not evenly divide into'
            warn2 = ' number of samples for DataSlice '+self.slicetype +'. '
            warn3 = 'When iterating over minibatches, leftovers are dropped.'
            leftover_warning = warn1+warn2+warn3 
            warnings.warn(leftover_warning)
    
    def avg_over_mbs(self,input_data_list):
        return np.sum(input_data_list*self.size_batches)/self.n_samples        
        
    def get_index_examples(self,n_each):        
        lbl_temp = np.asarray(self.lbl.eval())
        n_cat = np.unique(lbl_temp).size
        index = []        
        for i in range(n_cat):
            index += list(np.where(i==lbl_temp)[0][0:n_each])
        
        return np.asarray(index,dtype=INT)
        
    def randomize_mb(self):
        shuffle(self.index_order)
        
    def __iter__(self):
        self.current_index = 0
        return self
    
    def __next__(self):
        if self.current_index >= self.n_samples_iter:
            raise StopIteration
        else:
            # Only returns index to data/lbl
            stop = self.current_index + self.mbs
            index_slice = slice(self.current_index,stop)
            self.current_index = stop
            return self.index_order[index_slice]

#%%
def acc_log_reg_table(dataset,slicetype):
    """ 
    Just a convenient way to store accuracy of logistic regression
    applied to this dataset. This is a baseline that neural networks should
    match or beat to justify the extra work
    """     
    # Ran following sklearn code
    # logistic = linear_model.LogisticRegression(solver='newton-cg',
    #                                          multi_class = 'multinomial')
    # logistic = logistic.fit(data,lbl)
    # predict = logistic.predict(data) 
    # accuracy = 100.0*sum(int(pp==ll) for (pp,ll) in zip(predict,lbl))/data.shape[0]
    # Fit is always to train, but predict is modified to train,valid,test

    # here is what I have tested
    acc_dict = {
        'MNIST.train': 93.45,'MNIST.valid': 92.88,'MNIST.test': 92.56, 
        'MNIST_binary.train': 93.89,
        'MNIST_binary.valid': 90.72,
        'MNIST_binary.test': 86.24,
        'Overfit_Training.train': 100.0,
        'Overfit_Training.valid': 87.33,
        'Overfit_Training.test': 86.28,
        'Easy.train': 100.0,'Easy.valid': 100.0,'Easy.test': 99.95,
        'Noise.train': 58.86, 'Noise.valid': 51.69,'Noise.test': 48.90
        }
        
    lookup = dataset + '.' + slicetype
    
    if lookup not in acc_dict.keys():
        acc_log_reg = None
    else:
        acc_log_reg = acc_dict[lookup]        
        
    return acc_log_reg
           

#%%
def download_data(filename,source):
    """ Downloads needed datasets """
    
    if sys.version_info[0] == 2:
        from urllib import urlretrieve
    else:
        from urllib.request import urlretrieve

    print("Downloading {}".format(filename))
    urlretrieve(source + filename, '../data/'+filename)
    
#%%
def make_npz(data,lbl,filename):
    """ Save data and lbl to npz """
    filename = '../data/' + filename
    np.savez_compressed(filename,data=data,lbl=lbl)

#%%
def open_npz(filename):
    """ Open npz and return data and lbl """
    filename = '../data/' + filename
    with np.load(filename) as load_data:
        data = load_data['data']
        lbl = load_data['lbl']
        
    return (data,lbl)

#%% load mnist
def load_data_mnist():
    """
    Loads MNIST dataset of digits. Training has 50,000 examples, 
    while valid and test have 10,000 examples each    
    """
    
    filename = {'train.data': 'train-images-idx3-ubyte.gz',
                'train.lbl': 'train-labels-idx1-ubyte.gz',
                'valid.data': None,
                'valid.lbl': None,
                'test.data': 't10k-images-idx3-ubyte.gz',
                'test.lbl': 't10k-labels-idx1-ubyte.gz'}    
    
    npz = {'train': 'mnist_train.npz',
            'valid': 'mnist_valid.npz',
            'test': 'mnist_test.npz'}    
    
    url = 'http://yann.lecun.com/exdb/mnist/'            
                    
    # If needed, will download the data
    if not os.path.isfile('../data/'+filename['train.data']):
        for f in filename.values():
            if f is not None:
                download_data(f,url)
    
    # If does not exist, will make npz
    if not os.path.isfile('../data/'+npz['train']):
        
        def load_mnist_images(filename):
            filename = '../data/'+filename
            # Read the inputs in Yann LeCun's binary format.
            with gzip.open(filename, 'rb') as f:
                data = np.frombuffer(f.read(), np.uint8, offset=16)
            # The inputs are vectors now, we reshape them 
            # to monochrome 2D images, following the shape convention: 
            # (examples, channels, rows, columns)
            data = data.reshape(-1, 1, 28, 28)
            # The inputs come as bytes, we convert them to float32 
            # in range [0,1].
            return data / np.float32(255)

        def load_mnist_labels(filename):
            filename = '../data/'+filename
            # Read the labels in Yann LeCun's binary format.
            with gzip.open(filename, 'rb') as f:
                data = np.frombuffer(f.read(), np.uint8, offset=8)
            # The labels are vectors of integers now
            return data        
            
        data = load_mnist_images(filename['train.data'])
        lbl = load_mnist_labels(filename['train.lbl'])
        
        # We reserve the last 10000 training examples for validation.
        num = 10000
        train_data, valid_data = data[:-num], data[-num:]
        train_lbl, valid_lbl = lbl[:-num], lbl[-num:]
        
        test_data = load_mnist_images(filename['test.data'])
        test_lbl = load_mnist_labels(filename['test.lbl'])        
        
        # Sort data by categorical label
        index_sort = np.argsort(train_lbl)
        train_lbl = train_lbl[index_sort]
        train_data = train_data[index_sort]
        index_sort = np.argsort(valid_lbl)
        valid_lbl = valid_lbl[index_sort]
        valid_data = valid_data[index_sort]
        index_sort = np.argsort(test_lbl)
        test_lbl = test_lbl[index_sort]   
        test_data = test_data[index_sort]
        
        # Make npz for future loading
        make_npz(train_data,train_lbl,npz['train'])
        make_npz(valid_data,valid_lbl,npz['valid'])        
        make_npz(test_data,test_lbl,npz['test'])
    
    ########################################################
    # Now prepare return
    train_data,train_lbl = open_npz(npz['train']) 
    valid_data,valid_lbl = open_npz(npz['valid']) 
    test_data,test_lbl = open_npz(npz['test']) 
    
    datadict = {'train.data' : train_data,
                'train.lbl' : train_lbl,
                'valid.data' : valid_data,
                'valid.lbl' : valid_lbl,
                'test.data' : test_data,
                'test.lbl' : test_lbl}
    
    return datadict

#%%
def load_data_mnist_binary():
    """ This is MNIST with only 0,1 pixel values """    
    datadict = load_data_mnist()
    
    threshold = 0.5
    
    datadict['train.data'] = 1.0*(datadict['train.data']>threshold)    
    datadict['valid.data'] = 1.0*(datadict['valid.data']>threshold)
    datadict['test.data'] = 1.0*(datadict['test.data']>threshold)    
    
    return datadict

#%%
def load_data_mnist_binary_double():
    """ This is MNIST with only 0,1 pixel values """    
    datadict = load_data_mnist()
    
    threshold = 0.5
    
    data = 1.0*(datadict['train.data']>threshold)
    datadict['train.data'] = np.concatenate((data,data),axis=2) 
    data = 1.0*(datadict['valid.data']>threshold)
    datadict['valid.data'] = np.concatenate((data,data),axis=2) 
    data = 1.0*(datadict['test.data']>threshold) 
    datadict['test.data'] = np.concatenate((data,data),axis=2) 
    
    return datadict

#%% 
def load_data_overfit_train():
    """ 
    This is a small training set so you should get 100% accuracy on train 
    """
    
    datadict = load_data_mnist()
    
    N = datadict['train.data'].shape[0]
    keep = np.random.choice(N, 1000)
    datadict['train.data'] = datadict['train.data'][keep]
    datadict['train.lbl'] = datadict['train.lbl'][keep]
    
    return datadict

#%% 
def load_data_overfit_train_binary():
    """ 
    This is a small training set so you should get 100% accuracy on train 
    """
    
    datadict = load_data_mnist()   
    
    N = datadict['train.data'].shape[0]
    keep = np.random.choice(N, 1000)
    datadict['train.data'] = datadict['train.data'][keep]
    datadict['train.lbl'] = datadict['train.lbl'][keep]
    
    threshold = 0.5
    
    datadict['train.data'] = 1.0*(datadict['train.data']>threshold)    
    datadict['valid.data'] = 1.0*(datadict['valid.data']>threshold)
    datadict['test.data'] = 1.0*(datadict['test.data']>threshold)     
    
    return datadict


#%% 
def load_data_easy():
    """
    This is an easy train set so you should get very good results
    """
    
    datadict = load_data_mnist()    
    
    def keep_01(data,lbl):
        keep = np.where(np.logical_or((lbl == 0),(lbl == 1)))[0]
        rem = np.remainder(keep.size,100) # I assume minibatches divide into 100
        if rem>0:
            keep = keep[0:-rem]
        return (data[keep], lbl[keep])

    train_data,train_lbl = \
        keep_01(datadict['train.data'],datadict['train.lbl'])
    valid_data,valid_lbl = \
        keep_01(datadict['valid.data'],datadict['valid.lbl'])
    test_data,test_lbl = \
        keep_01(datadict['test.data'],datadict['test.lbl'])

    datadict = {'train.data' : train_data,
                'train.lbl' : train_lbl,
                'valid.data' : valid_data,
                'valid.lbl' : valid_lbl,
                'test.data' : test_data,
                'test.lbl' : test_lbl}
                
    return datadict

#%% 
def load_data_noise():
    """
    This is a fake dataset and should do around 50%
    """    
    
    datadict = load_data_mnist() 

    def make_rndm_lbl(data,lbl):
        keep = np.where(lbl == 1)[0]
        rem = np.remainder(keep.size,100) # I assume minibatches divide into 100
        if rem>0:
            keep = keep[0:-rem]
        data = data[keep]
        num = keep.size
        lbl = (1+np.sign(np.random.uniform(size=num)-0.5))/2
        
        return (data,lbl)
 
 
    train_data,train_lbl = \
        make_rndm_lbl(datadict['train.data'],datadict['train.lbl'])
    valid_data,valid_lbl = \
        make_rndm_lbl(datadict['valid.data'],datadict['valid.lbl'])
    test_data,test_lbl = \
        make_rndm_lbl(datadict['test.data'],datadict['test.lbl'])

    datadict = {'train.data' : train_data,
                'train.lbl' : train_lbl,
                'valid.data' : valid_data,
                'valid.lbl' : valid_lbl,
                'test.data' : test_data,
                'test.lbl' : test_lbl}
 
    return datadict
                                 

#%% load from a neural_network object
def load_data_neural_network(load_network_dict,hid_layer=None):
    """
    Loads data directly from a neural network. Passes original data into
    neural network and takes output activations as new data
    """
        
    nnet = load_network_dict['nnet']
    data = load_network_dict['data']
    
    
    if hid_layer is None:
        hid_layer = -1
    else:
        assert hid_layer>0 # assume visible is layer 0
        hid_layer -= 1
    train_data_ls = nnet.output_act(data.train.data)
    valid_data_ls = nnet.output_act(data.valid.data)
    test_data_ls = nnet.output_act(data.test.data)
    train_data = train_data_ls[hid_layer]        
    valid_data = valid_data_ls[hid_layer] 
    test_data = test_data_ls[hid_layer]         
    
        
    datadict = {'train.data' : train_data,
                'train.lbl' : data.train.lbl.eval(),
                'valid.data' : valid_data,
                'valid.lbl' : data.valid.lbl.eval(),
                'test.data' : test_data,
                'test.lbl' : data.test.lbl.eval()}

    return datadict