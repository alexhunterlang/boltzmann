#%%
import os
#import rbm
import dbm
#import dbn
import sys
import numpy as np

#%% parameters to search

lr_ls = [5e-3]
decay_ls = list(9.0/(250-10)*np.array([1]))
L1_ls = [0]
L2_ls = [1e-4]
p_drop_ls = [0.0, 0.2, 0.5]
IS_mf_ls = [True, False]

lr_name = ['005']
decay_name = ['9']
L1_name = ['0']
L2_name = ['0001']
p_drop_name = ['0', '20','50']
mf_name = ['T','F']

#%% other parameters
dataset = 'MNIST_binary'
n_epochs = 250 # {100} for pretraining, {500} for training
n_epochs_early = 10 # {10} mom_early and no decay    
n_h_ls = [500,1000] # {500} number of hidden units
mbs = 100 # size of minibatch, {100}
mom_early = 0.5 # {0.5},first n_epochs_early
mom_late = 0.9 # {0.9}, all remaining epochs
k = 5 # Number of CDk steps, {1} for pretraining, {5} for unsuper
#prob_drop = 0.5
#IS_mf = True # {True}, mean field or TAP
persistent = True # Standard CD vs persistent CD, {True} for unsuper
pretrain = True
network_to_load = None # filename of pretrained network or None
base_folder = '../results/dbm_59' # folder to save results to
keep_snapshots=True # unless debugging, print out lots of details
save_all = False # normal only save model when recording statistics

#%% code to run
for lr,lr_n in zip(lr_ls,lr_name):
    for d,d_n in zip(decay_ls,decay_name):
        for L1,L1_n in zip(L1_ls,L1_name):
            for L2,L2_n in zip(L2_ls,L2_name):
                for prob_drop,pn in zip(p_drop_ls,p_drop_name):
                    for IS_mf,mf_n in zip(IS_mf_ls,mf_name):
                        fname = '/lr'+lr_n+'_d'+d_n+'_L1_'+L1_n+'_L2_'+L2_n+'_pdrop_'+pn+'_mf_'+mf_n
                        save_folder = base_folder + fname
                        os.makedirs(save_folder)
                        # redirect system output
                        with open(save_folder + '/out.txt','w') as f:
                            sys.stdout = f
                            dbm_obj, data = dbm.main(dataset, n_epochs, n_epochs_early, n_h_ls, mbs,
                                                     lr, d, mom_early, mom_late, L1, L2, prob_drop, k, IS_mf,
                                                     persistent, pretrain, network_to_load, save_folder,keep_snapshots, save_all)                            
                             