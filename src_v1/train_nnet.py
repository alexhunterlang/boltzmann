"""
Training Neural Networks in Theano

Original Code Source: 
    [1] http://deeplearning.net/tutorial/rbm.html
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3
"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import timeit
import numpy as np
import theano
import theano.tensor as TT
import my_data
import paparazzi
import warnings

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

msg = "numpy.ndarray size changed, may indicate binary incompatibility"
# can safely ignore this warning
# see https://groups.google.com/forum/#!topic/theano-users/A__NVIBYMxA
warnings.filterwarnings("ignore", message=msg)

#%%

def train_nnet(data_input, data, nnet_obj, n_epochs, n_epochs_early,
               lr, lr_decay,  mom_early, mom_late,save_folder,
               keep_snapshots=True, save_all=False, data_lbl = None):
                   
    """
    Function that trains RBMs and monitors progress    
  
    :param data_input: theano input matrix
                    
    :param data: data object

    :param n_epochs: number of epochs used for training
    
    :param n_epochs_early: number of epochs with mom_early and no lr decay
    
    :param lr: learning rate used for training the neural network
    
    :param lr_decay: late_lr = lr/(1+lr_decay*(epoch-n_epochs_early))

    :param mom_early: coefficient of Nesterov momentum for early epochs 
    
    :param mom_late: coefficient of Nesterov momentum for late epochs
    
    :param save_folder: Path where results are saved
    
    :param keep_snapshots: whether to record all statistical details
    
    :param save_all: whether to save model at every epoch or only when 
                        recording statistical details
    """

    # Some basic checks
    assert isinstance(data, my_data.Data)
    assert (mom_early >= 0) and (mom_early < 1)
    assert (mom_late >= 0) and (mom_late < 1)

    if nnet_obj.train_class == 'unsuper':
        assert data_lbl is None
    elif nnet_obj.train_class == 'super':
        assert data_lbl is not None
    else:
        raise NotImplementedError

    start_tot_time = timeit.default_timer()

    # Calculate momentum for each mb
    n_mb = data.train.n_batches
    n_mb_tot = n_epochs*n_mb
    n_mb_early = n_epochs_early*n_mb
    mom_mb = mom_late*np.ones(n_mb_tot)
    n_e = np.min((n_mb_tot,n_mb_early))
    if mom_late > mom_early:
        # in early epochs, want log10 of vmax to grow linearly
        v_early = 1/(1-mom_early)
        v_late = 1/(1-mom_late)
        temp_mom = 1.0 - 10**-np.linspace(np.log10(v_early),
                                          np.log10(v_late),n_mb_early)
        mom_mb[0:n_e] = temp_mom[0:n_e]
    else:
        mom_mb[0:n_e] = mom_early*np.ones((n_e,))

    # Calculate lr for each mb
    lr_mb = lr*np.ones(n_mb_tot)
    if (lr_decay>0) and (n_epochs>n_epochs_early):
        ee = np.array(range(n_mb_tot-n_mb_early+1))
        lr_mb[n_mb_early-1:n_mb_tot] = lr/(1+lr_decay*ee/n_mb)

    # Hidden parameters
    # I don't plan on changing these, but one could in principle
    n_mom_ch = 2 # additional details for each new momentum
    n_details = 50 # how often to take detailed info  
    
    # allocate symbolic variables for the data
    index = TT.vector(name='index',dtype=INT)   # index to minibatch data
    current_mom = TT.scalar(name='current_mom',dtype=FLOAT)
    current_lr = TT.scalar(name='current_lr',dtype=FLOAT)
    
    # This makes an object to track useful statistics and make printouts
    papa = paparazzi.Paparazzi(nnet_obj,data,mom_mb,lr_mb,n_epochs_early,
                               save_folder,keep_snapshots)

    # update training to one step of pCDk
    cost, cost2, updates = nnet_obj.training_update(lr = current_lr,
                                                    mom = current_mom)

    # it is ok for a theano function to have no output
    # the purpose of train_rbm is solely to update the nnet parameters
    if nnet_obj.train_class == 'unsuper':
        givens = {data_input: data.train.data[index]}
    elif nnet_obj.train_class == 'super':
        givens = {data_input: data.train.data[index],
                  data_lbl: data.train.lbl[index]}
        IS_dropout = False
        valid_cost_fxn = theano.function(inputs = [],
                                     outputs = nnet_obj.cost(data_lbl,IS_dropout),
                                     givens = {data_input: data.valid.data,
                                              data_lbl: data.valid.lbl})

        valid_errors_fxn = theano.function(inputs = [],
                                      outputs = nnet_obj.errors(data_lbl),
                                      givens = {data_input: data.valid.data,
                                              data_lbl: data.valid.lbl})

    train_fxn = theano.function([index,current_lr,current_mom],
                                [cost,cost2],
                                updates=updates,
                                givens=givens,
                                name='train_fxn',
                                allow_input_downcast=True)

    # prep for training
    epoch_time, train_time = 0.0 , 0.0
    print('\n')
    
    # go through training epochs
    lr_counter = 0
    for epoch in range(n_epochs):
        # go through the training set
        start_epoch_time = timeit.default_timer()
        data.train.randomize_mb() # need to switch up minibatch partitioning
        all_cost, all_cost2 = [], []        
        
        # loop through training set
        for current_index in data.train:
            lr_now = lr_mb[lr_counter]
            mom_now = mom_mb[lr_counter]
            batch_cost, batch_cost2 = train_fxn(current_index, lr_now, mom_now)
            all_cost.append(batch_cost)
            all_cost2.append(batch_cost2)
            lr_counter += 1

        papa.stats_update(all_cost,all_cost2,epoch)    
        papa.text_epoch(epoch)    
        # stats_update and text_epoch as essential, don't count towards stat time
        train_time += timeit.default_timer() - start_epoch_time

        # Record detailed statistics for first epochs of each momentum
        # as well as other specified time and always the end
        want_details = (np.mod(epoch,n_details)==0)
        last_epoch = (epoch+1==n_epochs)
        IS_early_mom = (epoch<n_mom_ch)
        IS_late_mom = (epoch>=n_epochs_early) and (epoch<n_epochs_early+n_mom_ch)
        should_record = want_details or last_epoch or IS_early_mom or IS_late_mom

        # record detailed statistics
        if should_record and keep_snapshots:
            if nnet_obj.train_class == 'unsuper':
                valid_cost = None
                valid_errors = None
            elif nnet_obj.train_class == 'super':
                valid_cost = valid_cost_fxn()
                valid_errors = valid_errors_fxn()
            papa.snapshots_epoch(epoch,valid_cost,valid_errors)  
            
        # record batch level statistics
        if last_epoch or IS_early_mom or IS_late_mom and keep_snapshots:
            papa.snapshots_mbs(epoch)
        
        # save the model
        if should_record or save_all:
            out_folder = save_folder + '/saved_model'
            filename = 'model_{}'.format(epoch)
            nnet_obj.save_nnet(out_folder,filename)
        
        epoch_time += timeit.default_timer() - start_epoch_time

    # make plots of costs over epochs
    if keep_snapshots:
        papa.snapshots_final()

    print('Training updates took {:0.3f} minutes'.format(train_time / 60.0))
    text = 'Training statistics and snapshots took {:0.3f} minutes'    
    print(text.format((epoch_time-train_time) / 60.0))
    tot_time = timeit.default_timer() - start_tot_time
    print('Total runtime of {:0.3f} minutes'.format(tot_time / 60.0))
   