"""
Deep Boltzmann Machines in Theano

Original Code Source: 
    [1] http://deeplearning.net/tutorial/rbm.html
    [2] https://github.com/lisa-lab/pylearn2/blob/master/pylearn2/rbm_tools.py
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

The main heart of the code is from [1], while the AIS code is modified from [2]

Useful References:
[1] http://deeplearning.net/tutorial/rbm.html
        - see notes about scan and theano optimization for justification of
            returning pre-sigmoid activation
[2] Hinton Guide to RBM - https://www.cs.toronto.edu/~hinton/absps/guideTR.pdf
[3] Initialization - http://arxiv.org/abs/1312.6120
[4] Nesterov momentum - https://github.com/lisa-lab/pylearn2/issues/677
[5] Neal (2001) Annealed importance sampling
    https://link.springer.com/article/10.1023/A:1008923215028
[6] Salakhutdinov, Murray (2008) On the quantitative analysis of DBNs
    http://www.cs.toronto.edu/~rsalakhu/papers/dbn_ais.pdf
[7] http://www.cs.toronto.edu/~rsalakhu/rbm_ais.html
[8] Deep Boltzmann machines http://machinelearning.wustl.edu/
                            mlpapers/paper_files/AISTATS09_SalakhutdinovH.pdf
                            
GPU Timing: nh1=500,nh2=500,pCD5, mf_updates=25, n_epochs=500
Updates took 285.7 min for total of 318.09 min
TODO: update with new code
"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import numpy as np
import theano
import theano.tensor as TT
from theano.tensor.nnet import sigmoid
import os
import my_data
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams 
import warnings
import train_nnet
import dbn
import utils
from utils import binomial_sample, merge_dicts
#import keras.backend as K

FLOAT = theano.config.floatX # should be float32 for GPU
INT = 'int32' # again 32 bits for GPU

msg = "numpy.ndarray size changed, may indicate binary incompatibility"
# can safely ignore this warning
# see https://groups.google.com/forum/#!topic/theano-users/A__NVIBYMxA
warnings.filterwarnings("ignore", message=msg)

#%%
#%%
class Synapse(object):
    def __init__(self, n_in, n_out, index,
                 init_W = None, prob_drop = 0.0, L1 = 0.0, L2 = 0.0):
                
        self.n_in = n_in
        self.n_out = n_out
        self.index = index 
        self.name = str(index)+str(index+1)    
        self.prob_drop = prob_drop        
        self.L1 = L1
        self.L2 = L2
        
        self.IS_L1 = (self.L1>0.0)        
        self.IS_L2 = (self.L2>0.0)        
        
        self.L1_TT = TT.cast(self.L1,dtype=FLOAT)
        self.L2_TT = TT.cast(self.L2,dtype=FLOAT)
                
        # Make weight
        if init_W is None:
            # Random initialization to full rank matrix
            # Approximate implementation of ref [3]
            gain = 1
            U = np.random.normal(size=(n_in,n_in))/np.sqrt(n_in)
            V = np.random.normal(size=(n_out,n_out))/np.sqrt(n_out)    
            S = np.zeros((n_in,n_out))
            np.fill_diagonal(S,gain)
            init_W = np.array(U.dot(S).dot(V),dtype=FLOAT)
    
        name = 'W'+self.name
        self.W = theano.shared(value = init_W, name = name, borrow = True)  
        
        self.W_final = self.W*(1-TT.cast(self.prob_drop,dtype=FLOAT))
        
        # velocity of weight
        init_W = np.zeros((n_in,n_out), dtype=FLOAT)
        name = 'vel_W'+self.name
        self.vel_W = theano.shared(value = init_W, name = name, borrow = True)
        
        # best of wegiht
        name = 'best_W'+self.name
        self.best_W = theano.shared(value = init_W, name = name, borrow = True)
        
    #%%
    def update_best(self):
        self.best_W.set_value(self.W_final.eval())

    #%%
    def get_norm_cost(self, want_np=False):
        
        if self.IS_L1 and not self.IS_L2:
            cost = self.L1_TT*TT.sum(TT.abs_(self.W_final))     
        elif not self.IS_L1 and self.IS_L2:
            cost = 0.5*self.L2_TT*TT.sum(TT.pow(self.W_final,2))
        elif self.IS_L1 and self.IS_L2:
            cost = self.L1_TT*TT.sum(TT.abs_(self.W_final)) +\
                    0.5*self.L2_TT*TT.sum(TT.pow(self.W_final,2))
        else:
            cost = TT.cast(0.0,dtype=FLOAT)

        if want_np:
            cost = cost.eval()

        return cost
        
    #%%
    def output_W(self):
        return self.W_final.eval()   
       
    #%%
    def output_best(self):
        return self.best_W.get_value() 
            

#%%
#%%
class Layer(object):
    def __init__(self, n_in, index, layer_type, np_rng, theano_rng,
                 W_in = None, W_out = None, mbs = 100,
                 prob_drop = 0.0, IS_persist = True, nonlin = sigmoid,
                 init_b = None, init_persist = None, data = None,
                 IS_dbm_adj = False):
                
        assert layer_type in ['input','middle','output']

        # visible is 0, output is n-1
        # W_in is from lower layers to current layer
        # W_out is from current layer to higher layers

        self.W_in = W_in
        self.W_out = W_out

        self.n_in = n_in
        self.index = index 
        self.name = str(index)    
        self.layer_type = layer_type           
        self.nonlin = nonlin
        self.mbs = mbs
        self.np_rng = np_rng
        self.theano_rng = theano_rng
        self.prob_drop = prob_drop
        self.IS_persist = IS_persist        
        self.IS_dropout = (self.prob_drop>0.0)    
        self.IS_dbm_adj = IS_dbm_adj
        
        if layer_type == 'input':
            self.direction = 'down'
            assert self.W_in is None
            assert self.W_out is not None
        elif layer_type == 'middle':
            self.direction = 'both'
            assert self.W_in is not None
            assert self.W_out is not None
        elif layer_type == 'output':
            self.direction = 'up'
            assert self.W_in is not None
            assert self.W_out is None
        
        # create bias
        if init_b is None:
            # create shared variable for visible units bias
            if (layer_type=='input') and (data is not None):
                # tip from ref [2]
                data_matrix = data.train.data.get_value()
                # Assumed scale of data is between 0 and 1
                assert np.min(data_matrix) >= 0
                assert np.max(data_matrix) <= 1
                b_mean = np.mean(data_matrix,axis=0)
                eps = 10 ** -4  # no reason to be too extreme...
                b_mean[b_mean < eps] = eps
                b_mean[b_mean > 1 - eps] = 1 - eps
                init_b = np.array(np.log2(b_mean/(1-b_mean)), dtype=FLOAT)
            else:            
                init_b = np.zeros(n_in, dtype=FLOAT)
        
        name = 'b'+self.name
        self.b = theano.shared(value=init_b,name=name, borrow=True) 
        
        # velocity of bias
        init_b = np.zeros(n_in, dtype=FLOAT)
        name = 'vel_b'+self.name
        self.vel_b = theano.shared(value=init_b, name=name, borrow=True)
        
        # best of bias
        name = 'best_b'+self.name
        self.best_b = theano.shared(value=init_b, name=name, borrow=True)
        
        # persistant state of layer
        if self.IS_persist:
            init_p = np.zeros((mbs,n_in),dtype=FLOAT)
            name = 'persist'+self.name
            self.persist = theano.shared(value=init_p, name=name, borrow=True)
            name = 'best_persist'+self.name
            self.best_persist = theano.shared(value=init_p, name=name, borrow=True)
        
        if self.prob_drop>0.0:
            self.prob_drop = TT.cast(prob_drop,dtype=FLOAT)
            
            value = ((1-prob_drop)*np.ones((self.mbs,self.n_in))).astype(FLOAT)  
            name = 'p_keep' + self.name                             
            self.p_keep = theano.shared(value = value, name = name, borrow = True)
            
            init_sample_keep = binomial_sample(self.theano_rng,self.p_keep)
            value = init_sample_keep.eval().astype(FLOAT)
            name = 'mask' + self.name
            self.mask = theano.shared(value = value, name = name, borrow=True)
        
    #%%        
    def update_best(self):
        self.best_b.set_value(self.b.get_value(borrow=False))
        self.best_persist.set_value(self.persist.get_value(borrow=False))
    
    #%%
    def get_input(self, input_up=None, input_self=None, input_down=None,
                       IS_mf=True, direction=None, IS_dropout=False):
                
        # Some basic error checking and prep for activations
        assert direction in [None,'up','down']
        
        if direction == 'up':
            assert self.direction in ['up','both']
            assert IS_mf
        elif direction == 'down':
            assert self.direction in ['down','both']
            assert IS_mf
        elif direction is None:
            direction = self.direction

        if not IS_mf:
            assert input_self is not None
            
        if (not IS_dropout) and (self.IS_dropout):
            W_adj = (1-self.prob_drop)
        else:
            W_adj = 1.0
            
        # Controls which interactions to include    
        IS_up = True
        IS_down = True   
        if direction=='both':
            assert (input_up is not None) and (input_down is not None)
        elif direction=='up':
            assert (input_up is not None)
            IS_down = False            
        elif direction=='down':
            assert (input_down is not None)
            IS_up = False
            
        # Calculate activation input
        z = self.b
        if IS_up:
            data = input_up
            W = self.W_in*W_adj
            z += TT.dot(data, W)
            if not IS_mf:
                z += -0.5*TT.dot(data-data**2, W**2)*(input_self-0.5)
        if IS_down:
            data = input_down
            W = self.W_out.T*W_adj
            z += TT.dot(data,W)
            if not IS_mf:
               z += -0.5*TT.dot(data-data**2, W**2)*(input_self-0.5)
                
        # need to compensate for reduced input        
        IS_z_adj = (direction != 'both') and (self.layer_type=='middle')
        if IS_z_adj or self.IS_dbm_adj:
            z *= 2            
            
        return z
        
    #%%
    def get_output(self,z,IS_dropout=None):
        
        if IS_dropout is None:
            IS_dropout = self.IS_dropout
            
        if IS_dropout:
            prob = self.nonlin(z)*self.mask
        else:
            prob = self.nonlin(z) 
            
        return prob
        
    #%% 
    def get_output_persist(self):
        
        if self.IS_dropout:
            persist = self.persist*self.mask
        else:
            persist = self.persist
            
        return persist
        
    #%%
    def update_dropout(self,updates):
        
        updates[self.mask] = binomial_sample(self.theano_rng,self.p_keep)

        return updates        
        
    #%%
    def output_b(self):
        return self.b.get_value()  
       
    #%%
    def output_best_b(self):
        return self.best_b.get_value() 
        
    #%%
    def output_persist(self):
        return self.persist.get_value()  
       
    #%%
    def output_best_persist(self):
        return self.best_persist.get_value() 

#%%
#%%
class DBM(object):
    """ Deep boltzmann machine """
    def __init__(self, data_input, data, n_h_ls, 
                 mbs = 100, k = 5, L1 = 0.0, L2 = 0.0, IS_mf = True,
                 IS_persist = True, prob_drop = 0.0, np_rng = None, 
                 theano_rng = None, network_to_load=None,
                 dbm_pre = None):       
        
        self.train_class = 'unsuper'        
        
        # TODO: include way to load persist state
        assert type(data) is my_data.Data

        self.data_input = data_input
        self.mbs = mbs
        self.k = k
        self.L1 = L1
        self.L2 = L2
        self.IS_mf = IS_mf
        self.IS_persist = IS_persist
        self.prob_drop = prob_drop
        self.IS_dropout = (self.prob_drop > 0.0)
        self.nonlin = sigmoid
        
        assert dbm_pre in [None, 'bottom', 'middle', 'top']
        self.dbm_pre = dbm_pre
        if dbm_pre is not None:
            assert len(n_h_ls) == 1
            if dbm_pre == 'bottom':
                self.IS_dbm_adj_ls = [False, True]
            elif dbm_pre == 'middle':
                self.IS_dbm_adj_ls = [True, True]
            elif dbm_pre == 'top':
                self.IS_dbm_adj_ls = [True, False]
        else:
            self.IS_dbm_adj_ls = (len(n_h_ls)+1)*[False]
        
        
        # can change if want regular momentum or nesterov
        self.IS_nest = True
        
        # If k is large, match n_ups in positive stats to k
        if self.k>10:
            self.n_ups = self.k
        else:
            self.n_ups = 10
        
        # create a number generator
        if np_rng is None:
            np_rng = np.random.RandomState()
        if theano_rng is None:
            theano_rng = RandomStreams(np_rng.randint(2 ** 30))
        self.np_rng = np_rng
        self.theano_rng = theano_rng
        
        # Gets network ready to load
        if network_to_load is not None:    
            if type(network_to_load) is dbn.DBN:
                dbn_dict = network_to_load.output_nnet()
            elif isinstance(network_to_load,str):
                temp = np.load(network_to_load)
                dbn_dict = temp['dbn_dict'].item(0)
        
        # Figures out size of each layer
        self.n_v = data.n_input
        self.n_all = [self.n_v] + n_h_ls
        self.n_layers = len(self.n_all)
        self.n_output = n_h_ls[-1]
        self.n_h_ls = n_h_ls[:-1]

        # Decides if need to calculate norm during training
        self.n_samples = data.train.n_samples
        self.has_norm_cost = (L1>0.0)  or (L2>0.0)  
        self.batch_norm = TT.cast(1.0*self.mbs/self.n_samples, dtype=FLOAT)

        # Preps for pslike functions
        # Preps for cost functions
        index = data.train.get_index_examples(n_each=100) 
        self.xi_examples = TT.round(data.train.data[index])   
        self.xi = TT.round(data.train.data)
        
        
        # Create synapses     
        self.synapses_ls = []
        for i in range(len(self.n_all)-1):
            if network_to_load is not None:
                init_W = dbn_dict['W'+str(i)+str(i+1)]
            else:
                init_W = None
                
            synapse = Synapse(n_in = self.n_all[i], n_out = self.n_all[i+1],
                              index = i, init_W = init_W,
                              prob_drop = prob_drop, L1 = L1, L2 = L2)
            self.synapses_ls.append(synapse)
        
        # get lists of sets of parameters
        self.weight_ls = []
        self.weight_final_ls = []
        self.bias_ls = []
        self.vel_ls = []
        self.best_ls = []
        self.persist_ls = []
        for synapse in self.synapses_ls:
            self.weight_ls.append(synapse.W)
            self.weight_final_ls.append(synapse.W_final)
            self.vel_ls.append(synapse.vel_W)
            self.best_ls.append(synapse.best_W)
                
        # create neuron layers
        self.layers_ls = []
        temp_weight_ls = [None] + self.weight_ls + [None]
        for i in range(len(self.n_all)):
            if i==0:
                layer_type = 'input'
                layer_data = data
            elif i==len(self.n_all)-1:
                layer_type = 'output'
                layer_data = None
            else:
                layer_type = 'middle'
                layer_data = None
    
            if network_to_load is not None:
                init_b = dbn_dict['b'+str(i)]
            else:
                init_b = None                
                
            layer = Layer(self.n_all[i], i, layer_type, self.np_rng, self.theano_rng,
                          W_in = temp_weight_ls[i], W_out = temp_weight_ls[i+1],
                          mbs = self.mbs, prob_drop = self.prob_drop,
                          IS_persist = self.IS_persist, nonlin = self.nonlin,
                          init_b = init_b, init_persist = None, data = layer_data,
                          IS_dbm_adj = self.IS_dbm_adj_ls[i])
            self.layers_ls.append(layer)

        self.parts_ls = self.synapses_ls+self.layers_ls
               
        # Prepares for persistence
        if self.IS_persist:
            index = np.random.randint(low=0,high=self.n_samples,
                                      size=(self.mbs,))
            temp_data = data.train.data[index]   
            prob_ls = self.propup(temp_data, IS_dropout = False)
            prob_ls = [temp_data]+prob_ls
            for i in range(len(self.layers_ls)):
                layer = self.layers_ls[i]
                layer.persist.set_value(prob_ls[i].eval())

        # get lists of sets of parameters
        for layer in self.layers_ls:
            self.bias_ls.append(layer.b)
            self.vel_ls.append(layer.vel_b)
            self.best_ls.append(layer.best_b)
            if self.IS_persist:
                self.persist_ls.append(layer.persist) 
        self.params = self.weight_ls+self.bias_ls
           
    #%%            
    def update_best(self,epoch):
        ''' Records best parameters '''
        self.best_epoch = epoch
        for part in self.parts_ls:
            part.update_best()
            
    #%%
    def get_norm_cost(self,want_np=False):
        ''' Returns weight costs '''
        if want_np:
            cost = 0.0
        else:
            cost = TT.cast(0.0,dtype=FLOAT)
            
        for synapse in self.synapses_ls:
            cost += synapse.get_norm_cost(want_np)
        
        return cost

    #%% 
    def get_output_persist(self):
        
        persist_ls = []
        for layer in self.layers_ls:
            persist_ls.append(layer.get_output_persist())
            
        return persist_ls
        
    #%%
    def update_dropout(self,updates):
        
        for layer in self.layers_ls:
            updates = layer.update_dropout(updates)
            
        return updates
        
    #%%
    def update_persist(self,prob_model,updates):
        
        # retrains persistent state despite dropout
        for persist, p_m, layer in zip(self.persist_ls, prob_model,
                                       self.layers_ls):
            if self.IS_dropout:
                mask = layer.mask
                keep_old = persist*(-mask+1)
                updates[persist] = p_m*mask + keep_old
            else:
                updates[persist] = p_m
                    
        return updates
        
    #%%
    def free_energy(self, vis):
        ''' Function to compute the free energy of a visible sample '''
        
        prob_ls = self.propup(vis, IS_dropout = False)
        prob_ls = [vis] + prob_ls

        return self.free_energy_given_h(prob_ls, IS_dropout = False)        

    #%%
    def free_energy_given_h(self, prob_ls, IS_dropout = False):
        """ Function for free energy given visible sample and 
        activations of hidden layers        
        """

        z_ls = []
        prob_ls = prob_ls+[None]
        for i in range(len(self.layers_ls[1:])):
            layer = self.layers_ls[i+1]
            z = layer.get_input(input_up = prob_ls[i], 
                                     input_down = prob_ls[i+2],
                                     IS_mf=True, IS_dropout=IS_dropout)
            z_ls.append(z)
    
        z = TT.concatenate(z_ls,axis=1)
        vbias_term = TT.dot(prob_ls[0], self.bias_ls[0])
        hidden_term = TT.sum(TT.log(1 + TT.exp(z)), axis=1)
        
        return - hidden_term - vbias_term
    
    #%%
    def propup(self, vis, IS_dropout = False):
        """ Pass data up through network"""
        
        prob_ls = []  
        prob_up = vis            
        
        for layer in self.layers_ls[1:]:
            z = layer.get_input(input_up = prob_up, direction = 'up',
                                     IS_mf = True, IS_dropout = IS_dropout)
            prob_up = layer.get_output(z, IS_dropout)
            prob_ls.append(prob_up)               
                
        return prob_ls        
 
    #%%
    def parity_update(self, z_ls, prob_ls, sample_ls, start, IS_dropout,
                      IS_prob_input = True):
        ''' Updates either even or odd layers of synapses'''
        
        prob_out = [None]*len(prob_ls)        
        
        if IS_prob_input:
            input_ls = [None] + prob_ls + [None]
        else:
            sample_in_ls = []
            for p in prob_ls:
                s = binomial_sample(self.theano_rng, p)
                sample_in_ls.append(s)
            input_ls = [None] + sample_in_ls + [None]
        
        for i in range(start,len(input_ls)-2,2):
            layer = self.layers_ls[i]
            z = layer.get_input(input_up = input_ls[i], 
                                     input_self = input_ls[i+1], 
                                     input_down = input_ls[i+2],
                                     IS_mf = self.IS_mf,
                                     IS_dropout = IS_dropout) 
            prob = layer.get_output(z, IS_dropout)                                     
            sample = binomial_sample(self.theano_rng, prob)
            
            z_ls[i] = z
            prob_out[i] = prob
            sample_ls[i] = sample
            
        # Pass out the probs that don't change
        for i,p in enumerate(prob_out):
            if p is None:
                prob_out[i] = prob_ls[i]
        
        return z_ls, prob_out, sample_ls
        
    #%%
    def gibbs_odd_even_odd(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the odd hidden states'''

        prob_ls = list(args)
        z_ls = [None]*len(prob_ls)
        sample_ls = [None]*len(prob_ls)    
 
        # Update even
        z_ls, prob_ls, sample_ls = self.parity_update(
                                            z_ls, prob_ls, sample_ls, 0, 
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = False)
        
        # update odd
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 1,
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = False)

        return z_ls+prob_ls+sample_ls             
        

    #%%
    def gibbs_even_odd_even(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states'''
               
        prob_ls = list(args)               
        z_ls = [None]*len(prob_ls)
        sample_ls = [None]*len(prob_ls)    
 
        # Update odd
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 1,
                                            IS_dropout = False,
                                            IS_prob_input = True)
        
        # update even
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 0,
                                            IS_dropout = False,
                                            IS_prob_input = True)

        return z_ls+prob_ls+sample_ls
                
    #%%
    def gibbs_even_odd_even_given_v(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states
            but with visible state fixed '''
                  
        if self.IS_dropout:
            vis = self.data_input*self.layers_ls[0].mask
        else:
            vis = self.data_input                  
                  
        prob_ls = [vis] + list(args)         
               
        z_ls = [None]*len(prob_ls)
        sample_ls = [None]*len(prob_ls)    
 
        # Update odd
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 1,
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = True)
        
        # update even (not vis)
        z_ls, prob_ls, sample_ls = self.parity_update( 
                                            z_ls, prob_ls, sample_ls, 2,
                                            IS_dropout = self.IS_dropout,
                                            IS_prob_input = True)

        return z_ls[1:]+prob_ls[1:]+sample_ls[1:]

    #%%
    def training_update(self, lr = 0.01, mom = 0.9):
        """
        This functions implements one step of CD-k or PCD-k with 
        Nesterov momentum

        :param lr: learning rate used to train the RBM

        :param mom: momentum coefficient for Nesterov momentum

        Returns a proxy for the cost and the updates dictionary. The
        dictionary contains the update rules for weights and biases but
        also an update of the shared variable used to store the persistent
        chain and dropout masks, if used.
        """

        mom = TT.cast(mom, dtype = FLOAT)
        lr = TT.cast(lr, dtype = FLOAT)

        prob_data, updates = self.pos_stats()
        
        z_v_model, prob_model, updates = self.neg_stats(prob_data, updates)
         
        cost = self.batch_pseudo_cost(prob_data, prob_model)         
        
        constant_ls = prob_data+prob_model
        
        grads = TT.grad(cost, self.params, consider_constant = constant_ls)
        
        for g, param, vel in zip(grads, self.params, self.vel_ls):
            new_vel = mom*vel - lr*g
            updates[vel] = new_vel
            if self.IS_nest:
                # Nesterov - ref [4]
                updates[param] = param + mom*new_vel - lr*g 
            else:
                # standard
                updates[param] = param + new_vel 
         
        # get batch cost measures
        recon = self.batch_recon_cost(z_v_model)
        
        # update masks for next minibatch
        if self.IS_dropout:
            updates = self.update_dropout(updates)        
                    
        return recon, cost, updates

    #%%
    def pos_stats(self):
        # compute positive phase
    
        if self.IS_dropout:
            vis = self.data_input*self.layers_ls[0].mask
        else:
            vis = self.data_input         
        
        # use propup to get initial mf activations
        prob_ls = self.propup(vis, self.IS_dropout)
                
        output_ls = [None]*len(prob_ls) + prob_ls + [None]*len(prob_ls)       
                
        scan_out, updates = theano.scan(fn = self.gibbs_even_odd_even_given_v, 
                                outputs_info = output_ls, 
                                n_steps = self.n_ups, name = 'scan_pos')              
        
        prob_data = scan_out[len(prob_ls):2*len(prob_ls)]
        for i in range(len(prob_data)):
            temp = prob_data[i]
            prob_data[i] = temp[-1]
        
        # include visible in prob_data
        prob_data = [vis] + prob_data
        
        return prob_data, updates
        
    #%%
    def neg_stats(self,prob_data,updates):
               
        # decide how to initialize persistent chain:
        if self.IS_persist:
            # for PCD, we initialize from the old state of the chain
            chain_start = self.get_output_persist()
        else:
            # for CD, we use the newly generated hidden sample
            chain_start = prob_data
        
        # perform actual negative phase
        output_ls = [None]*len(chain_start)+chain_start+[None]*len(chain_start)

        scan_out, scan_updates = theano.scan(self.gibbs_odd_even_odd, 
                                        outputs_info = output_ls, 
                                        n_steps = self.k, name = 'scan_neg')

        # merge updates
        updates = merge_dicts(updates, scan_updates)

        # Ref [2] recommends to always use probs for update statistics
        prob_model = scan_out[len(chain_start):2*len(chain_start)]
        for i in range(len(prob_model)):
            temp = prob_model[i]
            prob_model[i] = temp[-1]

        temp = scan_out[0]
        z_v_model = temp[-1]
        
        if self.IS_persist:
            # Ref [2] recommends to always use probs for update statistics
            updates = self.update_persist(prob_model,updates)
        
        return z_v_model, prob_model, updates
        
    #%%
    def batch_recon_cost(self,z_v_model):
        
        cross_entropy = -TT.mean(TT.sum(
                self.data_input * TT.log(sigmoid(z_v_model)) +
                (1 - self.data_input) *
                    TT.log(1 - sigmoid(z_v_model)), axis=1))
        return cross_entropy
    
    #%%
    def batch_pseudo_cost(self, prob_data, prob_model):
        # Pseudo cost
            
        fe_data = TT.mean(self.free_energy_given_h(prob_data, self.IS_dropout)) 
        fe_model = TT.mean(self.free_energy_given_h(prob_model, self.IS_dropout)) 
        cost = fe_data - fe_model        
        
        if self.has_norm_cost:
            cost += self.get_norm_cost()*self.batch_norm
            
        return cost
 
    #%%
    def pseudo_likelihood_cost_examples(self, bit_i_idx, cost):
        """
        Stochastic approximation to the pseudo-likelihood
        
        Noisy, need to look at moving average of result.        
        """

        # calculate free energy for the given bit configuration
        fe_xi = self.free_energy(self.xi_examples)

        # flip bit x_i of matrix xi and preserve all other bits x_{\i}
        # Equivalent to xi[:,bit_i_idx] = 1-xi[:, bit_i_idx], but assigns
        # the result to xi_flip, instead of working in place on xi.
        xi_flip = TT.set_subtensor(self.xi_examples[:, bit_i_idx], 
                                   1 - self.xi_examples[:, bit_i_idx])

        # calculate free energy with bit flipped
        fe_xi_flip = self.free_energy(xi_flip)

        # equivalent to e^(-FE(x_i)) / (e^(-FE(x_i)) + e^(-FE(x_{\i})))
        #n_v_TT = TT.cast(self.n_v, dtype=FLOAT)
        cost = cost + TT.mean(TT.log(sigmoid(fe_xi_flip - fe_xi)))

        return cost
        
    #%%
    def pseudo_likelihood_cost(self, bit_i_idx, cost):
        """
        Stochastic approximation to the pseudo-likelihood
        
        Noisy, need to look at moving average of result.        
        """

        # calculate free energy for the given bit configuration
        fe_xi = self.free_energy(self.xi)

        # flip bit x_i of matrix xi and preserve all other bits x_{\i}
        # Equivalent to xi[:,bit_i_idx] = 1-xi[:, bit_i_idx], but assigns
        # the result to xi_flip, instead of working in place on xi.
        xi_flip = TT.set_subtensor(self.xi[:, bit_i_idx], 1 - self.xi[:, bit_i_idx])

        # calculate free energy with bit flipped
        fe_xi_flip = self.free_energy(xi_flip)

        # equivalent to e^(-FE(x_i)) / (e^(-FE(x_i)) + e^(-FE(x_{\i})))
        #n_v_TT = TT.cast(self.n_v, dtype=FLOAT)
        cost = cost + TT.mean(TT.log(sigmoid(fe_xi_flip - fe_xi)))

        return cost
        
    #%%
    def output_act(self,vis):
        """
        Given input visible data, returns output activations as numpy array
        """        
        prob_ls = self.propup(vis, IS_dropout = False)
        output_ls = []
        for p in prob_ls:
            output_ls.append(p.eval())
        
        return output_ls

    #%%
    def output_nnet(self):
        # Save arrays to a dictionary
        # This successfully preserves names for each array
                
        d_out = {}    
        for i,synapse in enumerate(self.synapses_ls):
            key = 'W'+str(i)+str(i+1)
            d_out[key] = synapse.output_W()
        for i,layer in enumerate(self.layers_ls):
            key = 'b'+str(i)
            d_out[key] = layer.output_b()
            if self.IS_persist:
                key = 'persist'+str(i)
                d_out[key] = layer.output_persist()
            
        return d_out

    #%%
    def save_nnet(self,save_folder,filename):
        """ save network to a file """

        d_out = self.output_nnet()        

        if not os.path.exists(save_folder):
            os.makedirs(save_folder)
        outfile = save_folder + '/' + filename
        np.savez_compressed(outfile, dbm_dict = d_out)

#%%
def main(dataset, n_epochs, n_epochs_early, n_h_ls, mbs,
         lr, lr_decay, mom_early, mom_late, L1, L2, prob_drop, k, IS_mf,
         persistent, pretrain, network_to_load, save_folder,keep_snapshots, save_all):
    """
    :param dataset: name of dataset (MNIST, RBM, etc see my_data.py)

    :param n_epochs: number of epochs used for training
    
    :param n_epochs_early: number of epochs with mom_early and no lr decay
    
    :param n_h_ls: number of hidden units per layer

    :param mbs: Size of minibatch

    :param lr: learning rate used for training the neural network
    
    :param lr_decay: late_lr = lr/(1+lr_decay*(epoch-n_epochs_early))

    :param mom_early: coefficient of Nesterov momentum for early epochs 
    
    :param mom_late: coefficient of Nesterov momentum for late epochs
    
    :param L1: L1 norm penalty
    
    :param L2: L2 norm penalty
    
    :param k: Number of steps for pCDk or CDk    
    
    :param IS_mf: Mean field or TAP
    
    :param persistent: True for pCDk, False for CDk
    
    :param pretrain: If true, pretrains with a DBN

    :param network_to_load: Network object to load
    
    :param save_folder: Path where results are saved
    
    :param keep_snapshots: whether to record all statistical details
    
    :param save_all: whether to save model at every epoch or only when 
                        recording statistical details
    """
     
     # Save parameters used in simulation
    filename = save_folder + '/parameters.txt'
    param_dict = {'nnet_type' : 'dbm', 'dataset' : dataset,
                  'n_epochs' : n_epochs, 'n_epochs_early' : n_epochs_early,
                  'n_h_ls' : n_h_ls, 'mbs' : mbs, 
                  'lr' : lr, 'lr_decay' : lr_decay, 'mom_early' : mom_early, 
                  'mom_late' : mom_late, 'L1' : L1, 'L2' : L2, 'k' : k,
                  'IS_mf' : IS_mf, 'persistent' : persistent,
                  'prob_drop' : prob_drop, 'network_to_load' : network_to_load}
    utils.save_dict(filename,param_dict)
    
    # Obtains data object
    data = my_data.Data(dataset,mbs)  

    if pretrain:
        assert network_to_load is None # this gets ignored
        # don't save all for pretraining
        # pretraining for 100 epochs is usually plenty
        dbn_folder = save_folder+'/pretrain_dbn'
        
        print('Pretraining DBN for initialization of DBM\n\n') 
        n_epochs_pretrain = 100
        lr_decay_pretrain = 0
        dbn_obj = dbn.main(dataset, n_epochs_pretrain, n_epochs_early,
                           n_h_ls, mbs, lr, lr_decay_pretrain,
                              mom_early, mom_late, L1, L2, 
                              prob_drop, k, IS_mf, persistent, 
                              None, dbn_folder, keep_snapshots, False, True)       
        
        network_to_load = dbn_obj
 
    print('Training DBM\n\n')    

    data_input = TT.matrix('data_input',dtype=FLOAT)  # rasterized images

    dbm_obj = DBM(data_input, data, n_h_ls, mbs, k , L1, L2, IS_mf,
                 persistent, prob_drop, None, None, network_to_load)

    train_nnet.train_nnet(data_input, data, dbm_obj, n_epochs, n_epochs_early,
                          lr, lr_decay,  mom_early, mom_late, save_folder,
                          keep_snapshots, save_all)
                                    
    return dbm_obj, data

#%%
if __name__ == '__main__':
    """ Trains a DBM """
    # The dataset that you choose or the given input will automatically 
    # set the dimension of the visible layer. The hidden layer size is a 
    # free parameter. Nesterov momentum is implemented.
    # If given no input weights / bias, implements random
    # orthogonal weight matrices. 
    # Regularization only applies to weights not biases.

    # assumed folder structure:
    # parent folder path = pf
    # pf/src - where this code and others live
    # pf/data - where datasets are stored
    # pf/results - where results will be stored
    
    # Parameters to input
    # If don't want some option, set to 0.0 or None, see below
    # number in {} is recommendation
    
    dataset = 'MNIST_binary' # see my_data for options 
    n_epochs = 500 # {100} for pretraining, {500} for training
    n_epochs_early = 10 # {10} mom_early ramping up and no decay 
    n_h_ls = [500,1000]
    mbs = 100 # {100} size of minibatch
    lr = 0.005 # {0.005} # Bengio notes best lr is around 1/2 of diverging lr 
    lr_decay = 100.0/490 #{9.0/490} later epochs only. See Note 1 below    
    mom_early = 0.5 # {0.5}, up to n_epochs_early, ramps up exponentially
    mom_late = 0.9 # {0.9}, after n_epochs_early
    L1 = 5e-5 # L1 penalty, {1e-5} see Note 2 below
    L2 = 5e-5 # L2 penalty, {1e-5} see Note 2 below
    prob_drop = 0.0
    k = 25 # Number of CDk steps, {1} for pretraining, {5} for unsuper
    IS_mf = True # {True}, mean field or TAP
    persistent = True # Standard CD vs persistent CD, {True} for unsuper
    pretrain = True # {True} pretrain DBN to initialize dbm
    network_to_load = None # filename of pretrained network or None
    save_folder = '../results/test_dbm' # folder to save results to
    keep_snapshots=True # unless debugging, print out lots of details 
    save_all = False # normal only save model when recording statistics

    # Note 1:
    # true_lr = lr/(1+lr_decay*(epoch-n_early))

    # Note 2
    # L1/L2 = 1e-3 seems too high, L1/L2=0.0 is also not optimal
    # But any combination of L1,L2 from [0.0, 1e-4, 1e-5] is similar

    #################################################

    dbm_obj, data = main(dataset, n_epochs, n_epochs_early, n_h_ls, mbs,
                         lr, lr_decay, mom_early, mom_late, L1, L2, prob_drop, 
                         k, IS_mf, persistent, pretrain, network_to_load, 
                         save_folder, keep_snapshots, save_all)